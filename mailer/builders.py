import decimal
import os
from abc import abstractmethod

from django.conf import settings
from django.core.mail import EmailMessage
from django.template import loader, Template


class BaseMailBuilder:
    TEMPLATE_NAME = None
    FROM_EMAIL = settings.EMAIL_HOST_USER

    @classmethod
    def get_template(cls):
        return cls.TEMPLATE_NAME

    @classmethod
    def _get_rendered_template(cls, context: dict, template: str = None) -> Template:
        if not template:
            template = cls.get_template()
        template = loader.get_template(template)
        return template.render(context)

    @classmethod
    @abstractmethod
    def build_message(cls, **kwargs) -> EmailMessage:
        """
        Method for building message template with provided kwargs
        :param kwargs: parameters for email
        :return: rendered email
        """


class SendEmailMessageToManagers(BaseMailBuilder):

    @classmethod
    def build_message(cls, order) -> EmailMessage:
        products = ''
        options = ''
        for item in order.product_items.all():
            products += '\n Название: ' + item.product.title + '\n Время аренды: ' + item.time + '\n Цена товара: ' + \
                        str(decimal.Decimal(item.price))

        for item in order.option_items.all():
            if order.option_items.all().last() == item:
                options += '\n Название: ' + item.option.title + '\n  Кол-во: ' + str(
                    item.quantity) + '\n  Цена: ' + str(
                    decimal.Decimal(item.price))
            else:
                options += '\n Название: ' + item.option.title + '\n  Кол-во:' + str(
                    item.quantity) + '\n  Цена:  ' + str(
                    decimal.Decimal(item.price))

        data = f'Новый заказ! \n\n' \
               f'Имя: {order.name}\n' \
               f'Контактный номер: {order.phone}\n' \
               f'Связаться через: {order.feedback_type}\n' \
               f'Сумма заказа: {order.total}\n' \
               f'Дата заказа: {order.created_at}\n\n' \
               f'Заказанный товар: {products}\n\n' \
               f'Заказанные доп опции: {options}\n'
        message = EmailMessage('Новый заказ!', data, to=['info@fotobudka-v-arendu.ru'])
        return message
