class EmailError(BaseException):
    default_message = 'Error sending email'
