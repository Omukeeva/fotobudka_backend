import logging

from django.core.mail import EmailMessage

from mailer.builders import SendEmailMessageToManagers
from mailer.sender import EmailSender

logger = logging.getLogger(__name__)


class MailerService:
    @classmethod
    def send_manager_email(cls, order):
        message = SendEmailMessageToManagers.build_message(order=order)
        email_sender = EmailSender([message])
        email_sender.start()
        logger.info(f"Sent order email to {'info@fotobudka-v-arendu.ru'}")

    @classmethod
    def send_feedback_email(cls, name: str, phone: str, feedback_type: str):
        message = f'Заявка на обратный звонок! \n\n' \
                  f'Имя: {name}\n' \
                  f'Контактный номер: {phone}\n' \
                  f'Связаться через: {feedback_type}\n'
        message = EmailMessage('Заявка на обратный звонок!', message, to=['info@fotobudka-v-arendu.ru'])
        email_sender = EmailSender([message])
        email_sender.start()
        logger.info(f"Sent order email to {'info@fotobudka-v-arendu.ru'}")
