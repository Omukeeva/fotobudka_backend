from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpRequest

from product.models import Product


class CartItem:
    def __init__(self, product: Product, quantity: int = 1, price=0, time='', time_price='', size='', size_price='', options=None):
        if options is None:
            options = []
        self.time = time
        self.time_price = time_price
        self.size = size
        self.size_price = size_price
        self.product = product
        self.quantity = quantity
        self.price = price
        self.options = options

    def to_dict(self):
        return {
            'product_id': self.product.id,
            'price': str(self.price),
            'quantity': self.quantity,
            'time': self.time,
            'time_price': self.time_price,
            'size': self.size,
            'size_price': self.size_price,
            'options': self.options,
        }

    @property
    def product_price_total(self):
        if self.size_price:
            return (int(self.time_price) + int(self.size_price)) * int(self.quantity)
        else:
            return int(self.time_price) * int(self.quantity)

    @property
    def total(self):
        option_price = 0
        for i in range(len(self.options)):
            option_price += int(self.options[i]['price'])

        if self.size_price:
            return (int(self.time_price) + int(self.size_price)) * int(self.quantity) + option_price
        else:
            return int(self.time_price) * int(self.quantity) + option_price

    @property
    def option_price(self):
        option_price = 0
        for i in range(len(self.options)):
            option_price += int(self.options[i]['price'])
        return option_price


class CartManager:

    def __init__(self, request: HttpRequest, session_key=None):
        self.session_key = session_key or settings.CART_SESSION_KEY
        self.session_favorite_key = session_key or settings.FAVORITES_SESSION_KEY
        self.session_sort_key = session_key or settings.SORT_SESSION_KEY
        self.session = request.session
        self._items_dict = {}
        self.product_model = Product
        self.favorite_list = list()
        self.sort = '-order'

        if self.session_key in self.session:
            cart_representation = self.session[self.session_key]
            ids_ = cart_representation.keys()
            queryset = self.get_queryset().filter(id__in=ids_)
            for product in queryset:
                item = cart_representation[str(product.pk)]
                self._items_dict[product.pk] = CartItem(
                    product, item['quantity'],
                    price=item['price'], time=item['time'], time_price=item['time_price'],
                    size=item['size'], size_price=item['size_price'], options=item['options']
                )

        if self.session_favorite_key in self.session:
            favorite_representation = self.session[self.session_favorite_key]
            ids_ = favorite_representation.keys()
            queryset = self.get_queryset().filter(id__in=ids_)
            for product in queryset:
                self.favorite_list.append(
                    Product.objects.get(id=product.pk)
                )
        if self.session_sort_key in self.session:
            sort_representation = self.session[self.session_sort_key]
            ids_ = sort_representation.values()
            self.sort = list(ids_)[0]

    def __contains__(self, product):
        """
        Checks if the given product is in the cart.
        """
        return product in self.products

    def update_session(self):
        """
        Serializes the cart data, saves it to session and marks session as modified.
        """
        self.session[self.session_key] = self.cart_serializable
        self.session.modified = True

    def update_favorite_session(self):
        """
        Serializes the cart data, saves it to session and marks session as modified.
        """
        self.session[self.session_favorite_key] = self.favorite_serializable
        self.session.modified = True

    def update_sort_session(self):
        """
        Serializes the cart data, saves it to session and marks session as modified.
        """
        self.session[self.session_sort_key] = {'1': self.sort}
        self.session.modified = True

    @property
    def items(self):
        """
        The list of cart items.
        """
        return self._items_dict.values()

    @property
    def items_quantity(self):
        """
        The list of cart items.
        """
        return sum([int(item.quantity) for item in self.items])

    @property
    def total(self):
        subtotal = 0
        option_price = 0
        for item in self.items:
            if item.size_price:
                subtotal += (int(item.time_price) + int(item.size_price)) * int(item.quantity)
            else:
                subtotal += int(item.time_price) * int(item.quantity)
            for i in range(len(item.options)):
                option_price += int(item.options[i]['price'])
        return subtotal + option_price

    @property
    def options_list(self):
        for item in self.items:
            return item.options

    @property
    def cart_serializable(self):
        """
        The serializable representation of the cart.
        For instance:
        {
            '1': {'product_pk': 1, 'quantity': 2, price: '9.99'},
            '2': {'product_pk': 2, 'quantity': 3, price: '29.99'},
        }
        Note how the product pk servers as the dictionary key.
        """
        cart_representation = {}
        for item in self.items:
            # JSON serialization: object attribute should be a string
            product_id = str(item.product.pk)
            cart_representation[product_id] = item.to_dict()
        return cart_representation

    @property
    def favorite_serializable(self):
        favorite_representation = {}
        for item in self.favorite_list:
            product_id = str(item.pk)
            favorite_representation[product_id] = product_id
        return favorite_representation

    @property
    def sort_serializable(self):
        sort_serializable = {
            'sort': self.sort
        }
        return sort_serializable

    def update_option(self, product: Product, option_id='', option_quantity='', option_price=''):
        if option_id:
            for i in range(len(self._items_dict[product.pk].options)):
                if self._items_dict[product.pk].options[i]['id'] == option_id:
                    self._items_dict[product.pk].options[i]['quantity'] = option_quantity
                    self._items_dict[product.pk].options[i]['price'] = option_price
        self.update_session()

    def update_quantity(self, product: Product, quantity: int = 1, ):
        self._items_dict[product.pk].quantity = quantity
        self.update_session()

    def update_size(self, product: Product, size='', size_price=''):
        self._items_dict[product.pk].size = size
        self._items_dict[product.pk].size_price = size_price
        self.update_session()

    def update_time(self, product: Product, time='', time_price=''):
        self._items_dict[product.pk].time = time
        self._items_dict[product.pk].time_price = time_price
        self.update_session()

    def add(self, product: Product, quantity: int = 1, price=0, time='', time_price='', size='', size_price='', options=None):
        """
        Adds or creates products in cart. For an existing product,
        the quantity is increased and the price is ignored.
        """
        quantity = int(quantity)
        if quantity < 1:
            raise ValueError('Quantity must be at least 1 when adding to cart')
        if product in self.products:
            self._items_dict[product.pk].quantity = quantity
            self._items_dict[product.pk].price = price
            self._items_dict[product.pk].time = time
            self._items_dict[product.pk].time_price = time_price
            self._items_dict[product.pk].size = size
            self._items_dict[product.pk].size_price = size_price
            self._items_dict[product.pk].options = options
        else:
            self._items_dict[product.pk] = CartItem(product, quantity, price, time, time_price, size, size_price, options)

        self.update_session()

    def add_options(self, product: Product, options=None):
        self._items_dict[product.pk].options = options
        self.update_session()

    def get_product_model(self):
        return self.product_model

    def get_queryset(self):
        product_model = self.get_product_model()
        queryset = product_model.objects.all()
        return queryset

    def remove_option(self, product, option_id=''):
        """
        Removes the product.
        """
        id_opt = ''
        if product in self.products:
            for i in range(len(self._items_dict[product.pk].options)):
                if self._items_dict[product.pk].options[i]['id'] == option_id:
                    id_opt = i
            self._items_dict[product.pk].options.pop(id_opt)
            self.update_session()

    def remove(self, product):
        """
        Removes the product.
        """

        if product in self.products:
            del self._items_dict[product.pk]
            self.update_session()

    def remove_single(self, product):
        """
        Removes a single product by decreasing the quantity.
        """
        if product in self.products:
            if self._items_dict[product.pk].quantity <= 1:
                # There's only 1 product left so we drop it
                del self._items_dict[product.pk]
            else:
                self._items_dict[product.pk].quantity -= 1
            self.update_session()

    def clear(self):
        """
        Removes all items.
        """
        self._items_dict = {}
        self.update_session()

    @property
    def items_serializable(self):
        """
        The list of items formatted for serialization.
        """
        return self.cart_serializable.items()

    @property
    def products(self):
        """
        The list of associated products.
        """
        return [item.product for item in self.items]

    @property
    def items_dict(self):
        return self._items_dict

    def add_favorite_item(self, product_id):
        try:
            product = Product.objects.get(id=product_id)
        except ObjectDoesNotExist:
            return False

        self.favorite_list.append(product)
        self.update_favorite_session()
        return True

    def has_in_favorites(self, product_id):
        for item in self.favorite_list:
            if int(item.id) == int(product_id):
                return True

    def remove_from_favorite(self, product_id):
        product = Product.objects.get(id=product_id)
        self.favorite_list.remove(product)
        self.update_favorite_session()
        return True

    def change_sort(self, sort):
        self.sort = sort
        self.update_sort_session()
        return True

    def remove_all_favorites(self):
        self.favorite_list = list()
        self.update_favorite_session()
