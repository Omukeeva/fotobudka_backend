from django.contrib import admin

# Register your models here.
from order.models import Order, OrderProductItem, OrderOptionsItem, Feedback


class OrderProductItemInline(admin.StackedInline):
    model = OrderProductItem
    extra = 1
    max_num = 1


class OrderOptionsItemInline(admin.StackedInline):
    model = OrderOptionsItem
    extra = 1


class OrderAdmin(admin.ModelAdmin):
    inlines = (OrderProductItemInline, OrderOptionsItemInline,)
    list_display = ('phone', 'created_at', 'status',)

    class Meta:
        model = Order


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('phone', 'created_at', 'is_active',)

    class Meta:
        model = Feedback


admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(Order, OrderAdmin)
