from django.contrib.auth.models import AbstractUser
from django.db import models


class Order(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    NEW = 0
    MODERATING = 1
    DELIVERED = 2
    CANCELED = 3

    STATUS_CHOICES = (
        (NEW, 'Новый заказ'),
        (MODERATING, 'Обрабатывается'),
        (DELIVERED, 'Обработан'),
        (CANCELED, 'Ошибка заказа')
    )

    name = models.CharField(max_length=100, verbose_name='Имя', null=True)
    phone = models.CharField(max_length=100, verbose_name='Номер телефона')
    feedback_type = models.CharField(max_length=100, verbose_name='Связаться через', null=True)
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания'
    )
    status = models.PositiveSmallIntegerField(
        default=0, choices=STATUS_CHOICES, null=True, blank=False, verbose_name='Статус заказа'
    )
    total = models.DecimalField(
        max_digits=15, decimal_places=2, verbose_name='Итого', default=0
    )

    def __str__(self):
        return self.phone


class OrderProductItem(models.Model):
    class Meta:
        verbose_name = 'Продукт заказа'
        verbose_name_plural = 'Продукт заказа'

    order = models.ForeignKey(
        to='Order', on_delete=models.SET_NULL, null=True,
        related_name='product_items'
    )
    product = models.ForeignKey(
        to='product.Product', on_delete=models.SET_NULL, null=True,
        related_name='order_products', verbose_name='Продукт заказа'
    )
    quantity = models.PositiveIntegerField(verbose_name='Кол-во', default=1)
    time = models.CharField(verbose_name='Время аренды', max_length=100)
    size = models.CharField(verbose_name='Размер', max_length=100, null=True, blank=True)
    price = models.DecimalField(
        max_digits=14, decimal_places=2, verbose_name='Цена', null=True
    )
    subtotal = models.DecimalField(verbose_name='Итого', max_digits=15, decimal_places=2, null=True)

    def __str__(self):
        return self.product.title


class OrderOptionsItem(models.Model):
    class Meta:
        verbose_name = 'Доп. опции заказа'
        verbose_name_plural = 'Доп. опции заказа'

    order = models.ForeignKey(
        to='Order', on_delete=models.SET_NULL, null=True,
        related_name='option_items'
    )
    option = models.ForeignKey(
        to='product.Options', on_delete=models.SET_NULL, null=True,
        related_name='order_options', verbose_name='Доп. опции заказа'
    )
    option_product = models.ForeignKey(
        to='OrderProductItem', on_delete=models.SET_NULL, null=True, verbose_name='Доп. услуга к ', related_name='order_option'
    )
    price = models.DecimalField(
        max_digits=14, decimal_places=2, verbose_name='Цена', null=True
    )
    quantity = models.PositiveSmallIntegerField(verbose_name='Количество', default=1)
    subtotal = models.DecimalField(verbose_name='Итого', max_digits=15, decimal_places=2, null=True)

    def __str__(self):
        return self.option.title


class Feedback(models.Model):
    class Meta:
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратная связь'

    name = models.CharField(max_length=255, verbose_name='Имя', null=True)
    phone = models.CharField(max_length=255, verbose_name='Номер телефона')
    feedback_type = models.CharField(max_length=100, verbose_name='Связаться через', null=True)
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания', null=True
    )
    is_active = models.BooleanField(
        verbose_name='Заявка обработана', default=False
    )

    def __str__(self):
        return self.phone
