import json

import requests
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, CreateView

from mailer.services import MailerService
from order.forms import OrderForm
from order.main import CartManager
from order.models import Order
from order.services.order_service import OrderService
from product.models import Product, Options


@method_decorator([csrf_exempt, ], name='dispatch')
class FavoriteActionView(View):

    @staticmethod
    def post(request):
        request_return = json.loads(request.body)
        product_id = request_return.get('product_id')
        cart = CartManager(request)

        if cart.has_in_favorites(product_id):
            cart.remove_from_favorite(product_id)
            result = None
        else:
            result = cart.add_favorite_item(product_id)

        response = dict(
            success=result if result is not None else True, count_favorite=len(cart.favorite_list)
        )

        if result is None:
            response['message'] = 'Removed'
        else:
            response['message'] = 'Success' if result else 'Failed'

        return JsonResponse(response)


@method_decorator([csrf_exempt, ], name='dispatch')
class CreateOrderView(CreateView):
    model = Order
    form_class = OrderForm
    template_name = 'basket.html'

    def get_cart(self):
        return CartManager(self.request)

    def form_valid(self, form):
        cart = self.get_cart()
        order = OrderService.create_new_order_from_cart(
            **form.cleaned_data, cart=cart
        )
        mainArray = []
        main = {
            'name': form['name'].data,
            'phone': form['phone'].data,
            'feedback_type': form['feedback_type'].data,
            'total': cart.total,
        }
        mainArray.append(main)

        for item in cart.items:
            order_items = {
                'product': item.product.title,
                'price': item.product_price_total,
                'time': item.time,
                'size': item.size,
                'quantity': item.quantity,
                'subtotal': item.product_price_total,
            }
            mainArray.append(order_items)
            if item.options:
                for i in range(len(item.options)):
                    order_option_items = {
                        'option': Options.objects.filter(id=item.options[i]['id']).first().title,
                        'price': int(item.options[i]['price']) / int(item.options[i]['quantity']),
                        'quantity': item.options[i]['quantity'],
                        'subtotal': item.options[i]['price']
                    }
                    mainArray.append(order_option_items)
        MailerService.send_manager_email(order)
        requests.post('https://fb.apihide.com/sites/handler-ac563ds.php', data=json.dumps(mainArray))
        order.save()
        cart.clear()
        return JsonResponse(dict(success=True, message='Вы оставили заявку'))

    def form_invalid(self, form):
        message = ""
        for item in form.errors:
            message += form.errors[item]
        return JsonResponse(dict(success=False, message=message))


@method_decorator([csrf_exempt, ], name='dispatch')
class AddToCartView(View):
    @staticmethod
    def post(request):
        request_return = json.loads(request.body)
        cart = CartManager(request)
        time = ''
        time_price = ''
        size = ''
        size_price = ''
        options = ''
        product = Product.objects.get(id=request_return.get('product_id'))
        price = request_return.get('price')
        if request_return.get('time'):
            time = request_return.get('time', '')
        if request_return.get('time_price'):
            time_price = request_return.get('time_price', '')
        if request_return.get('size'):
            size = request_return.get('size', '')
        if request_return.get('size_price'):
            size_price = request_return.get('size_price', '')
        if request_return.get('options'):
            options = request_return.get('options', None)

        quantity = request_return.get('quantity', 1)
        cart.add(product, quantity=quantity, price=price,
                 time=time, time_price=time_price, size=size, size_price=size_price, options=options)
        return JsonResponse(
            dict(message="Added", cart_quantity=len(cart.products), price=(int(product.price1) * int(quantity)), total_price=cart.total),
            status=200)


@method_decorator([csrf_exempt, ], name='dispatch')
class AddOptionsView(View):
    @staticmethod
    def post(request):
        request_return = json.loads(request.body)
        cart = CartManager(request)
        product = Product.objects.get(id=request_return.get('product_id'))
        options = request_return.get('options', None)
        cart.add_options(product, options=options)
        return JsonResponse(dict(message="Added", cart_quantity=len(cart.products), total_price=cart.total), status=200)


@method_decorator([csrf_exempt, ], name='dispatch')
class UpdateOptionView(View):
    @staticmethod
    def post(request):
        cart = CartManager(request)
        request_return = json.loads(request.body)
        product = Product.objects.get(id=request_return.get('product_id'))
        option_id = request_return.get('option_id')
        option_quantity = request_return.get('option_quantity')
        option_price = request_return.get('option_price')
        cart.update_option(product, option_id=option_id, option_quantity=option_quantity, option_price=option_price)
        return JsonResponse(dict(message="Update", cart_quantity=len(cart.products), total_price=cart.total), status=200)


@method_decorator([csrf_exempt, ], name='dispatch')
class UpdateTimeProductView(View):
    @staticmethod
    def post(request):
        cart = CartManager(request)
        request_return = json.loads(request.body)
        product = Product.objects.get(id=request_return.get('product_id'))
        time = request_return.get('time')
        time_price = request_return.get('time_price')
        cart.update_time(product, time=time, time_price=time_price)
        return JsonResponse(dict(
            message="Update", cart_quantity=len(cart.products),
            price=cart._items_dict[product.pk].product_price_total, total_price=cart.total),
            status=200)


@method_decorator([csrf_exempt, ], name='dispatch')
class UpdateSizeProductView(View):
    @staticmethod
    def post(request):
        cart = CartManager(request)
        request_return = json.loads(request.body)
        product = Product.objects.get(id=request_return.get('product_id'))
        size = request_return.get('size')
        size_price = request_return.get('size_price')
        cart.update_size(product, size=size, size_price=size_price)
        return JsonResponse(dict(
            message="Update", cart_quantity=len(cart.products),
            price=cart._items_dict[product.pk].product_price_total, total_price=cart.total),
            status=200)


@method_decorator([csrf_exempt, ], name='dispatch')
class RemoveToCartView(View):
    @staticmethod
    def post(request):
        request_return = json.loads(request.body)
        cart = CartManager(request)
        product = Product.objects.get(id=request_return.get('product_id'))
        cart.remove(product)

        return JsonResponse(
            dict(message="Remove", cart=cart.cart_serializable, cart_quantity=len(cart.products), total_price=cart.total),
            status=200)


@method_decorator([csrf_exempt, ], name='dispatch')
class RemoveOptionToCartView(View):
    @staticmethod
    def post(request):
        request_return = json.loads(request.body)
        cart = CartManager(request)
        product = Product.objects.get(id=request_return.get('product_id'))
        option_id = request_return.get('option_id')
        cart.remove_option(product, option_id=option_id)

        return JsonResponse(
            dict(message="Remove", cart=cart.cart_serializable, cart_quantity=len(cart.products), total_price=cart.total),
            status=200)


class ClearCartView(View):
    @staticmethod
    def get(request):
        cart = CartManager(request)
        cart.clear()
        return JsonResponse(dict(message="Clear"), status=200)


class FavoriteListView(TemplateView):
    template_name = 'favorites.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['products'] = CartManager(self.request).favorite_list
        return context


class BasketView(TemplateView):
    template_name = 'basket.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['product'] = CartManager(self.request).products
        return context
