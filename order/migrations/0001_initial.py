# Generated by Django 2.2.6 on 2021-05-21 10:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=255, verbose_name='Номер телефона')),
                ('feedback_type', models.CharField(max_length=100, null=True, verbose_name='Связаться через')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата создания')),
                ('is_active', models.BooleanField(default=False, verbose_name='Заявка обработана')),
            ],
            options={
                'verbose_name': 'Обратная связь',
                'verbose_name_plural': 'Обратная связь',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=100, verbose_name='Номер телефона')),
                ('feedback_type', models.CharField(max_length=100, null=True, verbose_name='Связаться через')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'Новый заказ'), (1, 'Обрабатывается'), (2, 'Обработан'), (3, 'Ошибка заказа')], default=0, null=True, verbose_name='Статус заказа')),
                ('total', models.DecimalField(decimal_places=2, default=0, max_digits=15, verbose_name='Итого')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
        migrations.CreateModel(
            name='OrderProductItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.CharField(max_length=100, verbose_name='Время аренды')),
                ('price', models.DecimalField(decimal_places=2, max_digits=14, null=True, verbose_name='Цена')),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='product_order_items', to='order.Order')),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='product_order', to='product.Product', verbose_name='Продукт заказа')),
            ],
            options={
                'verbose_name': 'Продукт заказа',
                'verbose_name_plural': 'Продукт заказа',
            },
        ),
        migrations.CreateModel(
            name='OrderOptionsItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveSmallIntegerField(verbose_name='Количество')),
                ('price', models.DecimalField(decimal_places=2, max_digits=14, null=True, verbose_name='Цена')),
                ('option', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='option_order', to='product.Options', verbose_name='Доп. опции заказа')),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='option_order_items', to='order.Order')),
            ],
            options={
                'verbose_name': 'Доп. опции заказа',
                'verbose_name_plural': 'Доп. опции заказа',
            },
        ),
    ]
