
from django.core.exceptions import FieldError
from django.db import IntegrityError

from order.main import CartManager
from order.models import Order, OrderProductItem, OrderOptionsItem
from product.models import Options


class OrderService:
    model = Order

    @classmethod
    def create_new_order_from_cart(cls, name: str, phone: str, feedback_type: str,  cart: CartManager):
        try:
            order = cls.model.objects.create(
                name=name,
                phone=phone,
                feedback_type=feedback_type,
                total=cart.total,
            )
            order_items_to_create = []
            order_option_items_to_create = []

            for item in cart.items:
                order_items = OrderProductItem(
                    order=order,
                    product=item.product,
                    price=item.product_price_total,
                    time=item.time,
                    size=item.size,
                    quantity=item.quantity,
                    subtotal=item.product_price_total,
                )
                order_items_to_create.append(order_items)
                if item.options:
                    for i in range(len(item.options)):
                        order_option_items = OrderOptionsItem(
                            order=order,
                            option_product=OrderProductItem.objects.filter(product=item.product).first(),
                            option=Options.objects.filter(id=item.options[i]['id']).first(),
                            price=int(item.options[i]['price']) / int(item.options[i]['quantity']),
                            quantity=item.options[i]['quantity'],
                            subtotal=item.options[i]['price']
                        )
                        order_option_items_to_create.append(order_option_items)

            OrderOptionsItem.objects.bulk_create(order_option_items_to_create, ignore_conflicts=True)
            OrderProductItem.objects.bulk_create(order_items_to_create)
            return order
        except IntegrityError as e:
            raise Exception("Заказ с заданными параметрами уже существует")
        except FieldError:
            raise Exception("Заданные параметры не подходят для заказа")

    @classmethod
    def get(cls, **kwargs):
        return cls.model.objects.get(**kwargs)
