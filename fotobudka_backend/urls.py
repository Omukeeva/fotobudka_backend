"""fotobudka_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
from django.views.generic import TemplateView

from fotobudka_backend import settings
from main.sitemaps import StaticSiteMap, CategorySitemap, NewsSitemap, ProductSitemap
from main.views import (
    IndexPage, CategoryDetailView, feedback_create_view,
    NewsList, NewsDetail, rss_view,
    max_price_product_sort, min_price_product_sort, ProductDetailView, pagination, search_function, products_view)
from order.views import (
    FavoriteActionView, FavoriteListView, AddToCartView, RemoveToCartView,
    RemoveOptionToCartView, ClearCartView, CreateOrderView, BasketView,
    UpdateTimeProductView, UpdateSizeProductView, AddOptionsView, UpdateOptionView,
)

sitemaps = {
    'static': StaticSiteMap(),
    'category': CategorySitemap(),
    'news': NewsSitemap(),
    'product': ProductSitemap(),
}
urlpatterns = [
    path('admin/', admin.site.urls),
    path('jet/', include('jet.urls', 'jet')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', IndexPage.as_view(), name='main'),
    path('favorites', FavoriteListView.as_view(), name='favorites'),
    path('favorite/', FavoriteActionView.as_view(), name='favorite'),
    path('basket', BasketView.as_view(), name='basket'),
    path('order/', CreateOrderView.as_view(), name='order'),
    path('feedback/', feedback_create_view, name='feedback'),
    path('news', NewsList.as_view(), name='news'),
    path('news/<slug>', NewsDetail.as_view(), name='news_detail'),
    path('rss', rss_view, name='rss'),
    path('products_xml', products_view, name='products_xml'),
    path('sort_max_price/', max_price_product_sort, name='sort_max_price'),
    path('pagination/', pagination, name='pagination'),
    path('sort_min_price/', min_price_product_sort, name='sort_min_price'),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"), ),
    path('add_cart/', AddToCartView.as_view(), name='add_cart'),
    path('add_options/', AddOptionsView.as_view(), name='add_options'),
    path('remove_cart/', RemoveToCartView.as_view(), name='remove_cart'),
    path('remove_option_cart/', RemoveOptionToCartView.as_view(), name='remove_option_cart'),
    path('clear_cart/', ClearCartView.as_view(), name='clear_cart'),
    path('product_time_update/', UpdateTimeProductView.as_view(), name='product_time_update'),
    path('product_size_update/', UpdateSizeProductView.as_view(), name='product_size_update'),
    path('option_update/', UpdateOptionView.as_view(), name='option_update'),
    path('search_product/', search_function, name='search_product'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL,
                      document_root=settings.STATIC_ROOT)

urlpatterns += [
    path('<slug>', CategoryDetailView.as_view(), name='category'),
    path('<str:category_slug>/<slug>', ProductDetailView.as_view(), name='product'),
]

handler404 = 'main.views.handler404'
