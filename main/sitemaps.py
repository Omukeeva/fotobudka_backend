from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from main.models import Portfolio
from product.models import Category, Product


class StaticSiteMap(Sitemap):
    priority = 1.0

    def items(self):
        return ['main', 'news']

    def location(self, obj):
        return reverse(obj)


class CategorySitemap(Sitemap):
    priority = 1.0

    def items(self):
        return Category.objects.all()

    def location(self, obj):
        return reverse('category', kwargs={'slug': obj.slug})


class ProductSitemap(Sitemap):
    priority = 1.0

    def items(self):
        return Product.objects.all()

    def location(self, obj):
        if obj.category_slug:
            return reverse('product', kwargs={'category_slug': obj.category_slug.slug, 'slug': obj.slug})
        else:
            return reverse('product', kwargs={'category_slug': obj.category.first().slug, 'slug': obj.slug})


class NewsSitemap(Sitemap):
    priority = 0.8

    def items(self):
        return Portfolio.objects.all()

    def location(self, obj):
        return reverse('news_detail', kwargs={'slug': obj.slug})
