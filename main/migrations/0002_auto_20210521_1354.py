# Generated by Django 2.2.6 on 2021-05-21 10:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='advantages',
            name='category',
        ),
        migrations.RemoveField(
            model_name='advantages',
            name='is_main',
        ),
    ]
