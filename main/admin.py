from django.contrib import admin
from django.utils.html import format_html
from mptt.admin import DraggableMPTTAdmin

from main.models import (SliderPortfolio, Portfolio, Advantages, FAQMain, Discount, MetaPortfolio, Banner, Images, Presentation)


class SliderPortfolioInline(admin.StackedInline):
    model = SliderPortfolio
    extra = 1

    def image_tag(self, obj):
        return format_html('<img style="width: 50px; height: 50px;" src="{}" />'.format(obj.image.url))

    image_tag.short_description = 'Картинка'
    readonly_fields = ['image_tag']
    fields = (('image', 'image_tag'), 'alt')


class MetaPortfolioInline(admin.StackedInline):
    model = MetaPortfolio
    extra = 1
    max_num = 1


class PortfolioAdmin(admin.ModelAdmin):
    inlines = (SliderPortfolioInline, MetaPortfolioInline,)
    prepopulated_fields = {"slug": ('title',)}

    class Meta:
        model = Portfolio

    def image_tag(self, obj):
        return format_html('<img style="width: 50px; height: 50px; object-fit: cover;" src="{}" />'.format(obj.image.url))

    image_tag.short_description = 'Картинка'
    readonly_fields = ['image_tag']
    fields = ('category', 'title', 'date', ('image', 'image_tag'), 'alt', 'description', 'slug')


admin.site.register(Portfolio, PortfolioAdmin)
admin.site.register(FAQMain)
# admin.site.register(Advantages)
# admin.site.register(Discount)
# admin.site.register(Banner)
# admin.site.register(Images)
admin.site.register(Presentation)
