from urllib.parse import urlparse

from django import template
from django.urls import reverse

from order.main import CartManager
from product.models import Options, Category

register = template.Library()


@register.simple_tag
def get_absolute_url(request, image):
    return request.build_absolute_uri(image.url)


@register.simple_tag
def get_reverse_url(url_string: str, **kwargs):
    return reverse(url_string, kwargs={**kwargs})


@register.simple_tag
def slider_order(slider):
    return slider.order_by('-order')


@register.simple_tag
def category_order(category):
    return category.order_by('-order')


@register.simple_tag
def slider_product(slider):
    return slider.order_by('order')[:5]


@register.filter
def in_favorite(request, product_id):
    cart = CartManager(request)
    return cart.has_in_favorites(product_id)


@register.simple_tag
def count_favorite(request):
    cart = CartManager(request)
    return len(cart.favorite_list)


@register.simple_tag
def in_cart(request, product):
    cart = CartManager(request)
    return cart.__contains__(product)


@register.simple_tag
def option_in_cart(request, product, option_id):
    cart = CartManager(request)
    if cart.__contains__(product):
        test = cart.items_dict[product.pk]
        answer = False
        for i in range(len(test.options)):
            if str(option_id) == str(test.options[i]['id']):
                answer = True
        return answer


@register.simple_tag
def option_item_quantity_in_cart(request, product, option_id):
    cart = CartManager(request)
    if cart.__contains__(product):
        test = cart.items_dict[product.pk]
        answer = {}
        for i in range(len(test.options)):
            if str(option_id) == str(test.options[i]['id']):
                answer = test.options[i]['quantity']
        return answer


@register.simple_tag
def option_item_price_in_cart(request, product, option_id):
    cart = CartManager(request)
    if cart.__contains__(product):
        test = cart.items_dict[product.pk]
        answer = {}
        for i in range(len(test.options)):
            if str(option_id) == str(test.options[i]['id']):
                answer = test.options[i]['price']
        return answer


@register.simple_tag
def product_in_cart(request, product):
    cart = CartManager(request)
    if cart.__contains__(product):
        return cart.items_dict[product.pk]


@register.simple_tag
def option_title(option_id):
    return Options.objects.filter(id=option_id)


category_history = None


@register.simple_tag
def get_prev_url(request, product_id):
    referer = request.META.get('HTTP_REFERER')
    if referer:
        my_path = urlparse(referer).path.replace("/", "")
        global category_history
        category = Category.objects.filter(product__id=product_id, slug=my_path).first()
        if category:
            category_history = my_path
            return category
        elif Category.objects.filter(product__id=product_id, slug=category_history).first():
            return Category.objects.filter(product__id=product_id, slug=category_history).first()
        else:
            return None
