from PIL import Image
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files import File
from django.db import models
from imagekit.models import ImageSpecField
from mptt.models import MPTTModel
from pilkit.processors import ResizeToFill
from io import BytesIO


# def compress(image):
#     im = Image.open(image)
#     im_io = BytesIO()
#     if im.mode != 'RGBA':
#         im.save(im_io, 'JPEG', quality=85)
#         new_image = File(im_io, name=image.name)
#         return new_image
#     else:
#         return image


class FAQMain(models.Model):
    class Meta:
        verbose_name = 'Вопросы и ответы на главной'
        verbose_name_plural = 'Вопросы и ответы на главной'

    title = models.CharField(max_length=255, verbose_name='Вопрос')
    description = RichTextUploadingField(verbose_name='Ответ')

    def __str__(self):
        return self.title


class Advantages(models.Model):
    class Meta:
        verbose_name = 'Преимущества'
        verbose_name_plural = 'Преимущества'

    title = models.CharField(max_length=255, verbose_name='Название')
    text = models.TextField(verbose_name='Описание')
    image = models.FileField(verbose_name='Иконка')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return self.title


class SliderPortfolio(models.Model):
    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайдер'

    portfolio = models.ForeignKey(
        to='Portfolio', on_delete=models.SET_NULL, related_name='slider',
        verbose_name='Портфолио', null=True
    )
    image = models.ImageField(
        verbose_name='Картинка',
        upload_to='slider_images', null=True
    )
    image_crop = ImageSpecField(
        source='image', processors=[ResizeToFill(550, 550)],
        format='JPEG', options={'quality': 100}
    )
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return 'Слайдер - ' + str(self.pk)


class MetaPortfolio(models.Model):
    class Meta:
        verbose_name = 'Мета для Портфолио'
        verbose_name_plural = 'Мета для Портфолио'

    title = models.CharField(max_length=255, verbose_name='Мета title')
    description = models.TextField(verbose_name='Мета description')
    category = models.ForeignKey(
        to='Portfolio', on_delete=models.SET_NULL, verbose_name='Портфолио',
        related_name='meta', null=True
    )

    def __str__(self):
        return self.title


class Portfolio(models.Model):
    class Meta:
        verbose_name = 'Портфолио'
        verbose_name_plural = 'Портфолио'

    category = models.ForeignKey(
        to='product.Category', on_delete=models.SET_NULL, verbose_name='Категория',
        related_name='portfolio', null=True, blank=True
    )
    title = models.CharField(max_length=255, verbose_name='Название')
    date = models.DateField(verbose_name='Дата')
    image = models.ImageField(
        verbose_name='Главная картинка',
        upload_to='portfolio_images', null=True
    )
    image_crop = ImageSpecField(
        source='image', processors=[ResizeToFill(360, 220)],
        format='JPEG', options={'quality': 100}
    )
    image_crop_big = ImageSpecField(
        source='image', processors=[ResizeToFill(550, 550)],
        format='JPEG', options={'quality': 100}
    )
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    description = RichTextUploadingField(verbose_name='Текст')
    slug = models.SlugField(max_length=255, unique=True, verbose_name='Слаг', null=True, blank=True)

    def __str__(self):
        return self.title


class Discount(models.Model):
    class Meta:
        verbose_name_plural = 'Скидки и акции'
        verbose_name = 'Скидки и акции'

    title = models.CharField(verbose_name='Название', max_length=255)
    image = models.FileField(verbose_name='Картинка', upload_to='discount_images')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    in_main = models.BooleanField(verbose_name='Показать на главной')

    def __str__(self):
        return self.title


class Banner(models.Model):
    class Meta:
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннер'

    title = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Текст')
    image = models.FileField(verbose_name='Картинка', upload_to='banner')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    video = models.URLField(verbose_name='Ссылка на видео', blank=True)
    is_main = models.BooleanField(verbose_name='Главный баннер')
    btn_text = models.CharField(verbose_name='Текст для ссылки на категорию', max_length=255, null=True, blank=True, help_text='Текст должен быть таким "Выбрать фотобудку"/"Выбрать фотозону"/и т.д.')
    link = models.ForeignKey(to='product.Category', verbose_name='Ссылка на категорию', on_delete=models.SET_NULL, null=True, related_name='banner', blank=True)

    def __str__(self):
        return self.title


class Images(models.Model):
    class Meta:
        verbose_name = 'Картинка в блок "Чем мы занимаемся?"'
        verbose_name_plural = 'Картинки в блок "Чем мы занимаемся?"'

    image = models.FileField(verbose_name='Картинка', upload_to='banner')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return 'Картинка № ' + str(self.pk)

    # def save(self, *args, **kwargs):
    #     new_image = compress(self.image)
    #     self.image = new_image
    #     super().save(*args, **kwargs)


class Presentation(models.Model):
    class Meta:
        verbose_name = 'Презентация'
        verbose_name_plural = 'Презентация'

    file = models.FileField(verbose_name='Презентация', upload_to='presentation')

    def __str__(self):
        return 'Презентация'
