from django.urls import resolve

from main.models import Portfolio, Advantages, Presentation
from order.main import CartManager
from product.models import Category, Product


def base(request):
    context = {
        'category_list': Category.objects.all(),
        'advantages': Advantages.objects.filter()[:6],
        'category_header': Category.objects.filter(header=True, parent__isnull=True),
        'category_event': Category.objects.filter(parent__isnull=False),
        'change_category': Category.objects.filter(header=True, parent__isnull=False),
        'portfolio': Portfolio.objects.all().order_by('-date'),
        'products': Product.objects.all(),
        'cart': CartManager(request),
        'recommendation': Product.objects.filter(popular=True),
        'sort_value': CartManager(request).sort,
        'presentation': Presentation.objects.first()

    }
    return context


def get_current_path_name(request):
    resolved = resolve(request.path_info)
    return {
        'path_name': resolved.url_name
    }
