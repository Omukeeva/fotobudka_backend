import json

import requests
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView, ListView

from mailer.services import MailerService
from main.models import FAQMain, Discount, Portfolio, MetaPortfolio, Banner, Images, Presentation
from order.main import CartManager
from order.models import Feedback
from product.models import Category, MetaCategory, FAQ, Product, Slider, AboutCategory, WorkCategory, MetaProduct, Characteristic, Brand, Event, BrandExample, ResultBlock, SeoForCategory, SeoProduct


class IndexPage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['banner'] = Banner.objects.all().order_by('-is_main')
        if len(Images.objects.all()) < 4:
            context['image'] = Images.objects.first()
        else:
            context['images'] = Images.objects.all()[:4]
        context['faq_main'] = FAQMain.objects.all()
        context['discount'] = Discount.objects.filter(in_main=True)[:2]
        context['popular'] = Product.objects.filter(popular=True)
        return context


class CategoryDetailView(DetailView):
    model = Category
    context_object_name = 'category'
    template_name = 'products.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        products_list = Product.objects.filter(
            category__id=self.get_object().id
        ).order_by(CartManager(self.request).sort)

        page = self.request.GET.get('page', 1)

        paginator = Paginator(products_list, 12)

        try:
            products = paginator.page(page)
            page_nav = page
        except PageNotAnInteger:
            products = paginator.page(1)
            page_nav = 0
        except EmptyPage:
            products = []
            page_nav = 'last'

        context['seo'] = SeoForCategory.objects.filter(
            category__id=self.get_object().id
        ).order_by('order')
        context['about'] = AboutCategory.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['event'] = Event.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['brand_example'] = BrandExample.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['result_block'] = ResultBlock.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['work'] = WorkCategory.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['meta'] = MetaCategory.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['faq'] = FAQ.objects.filter(
            category__id=self.get_object().id
        )
        context['products'] = products
        context['page_nav'] = page_nav
        context['slider'] = Slider.objects.filter(
            product__category__id=self.get_object().id
        ).order_by('order')
        return context


def pagination(request):
    category_id = request.GET.get('category_id', None)
    page = request.GET.get('page', 1)

    products_list = Product.objects.filter(category__id=category_id).order_by(CartManager(request).sort)

    paginator = Paginator(products_list, 12)
    try:
        products = paginator.page(page)
        page_nav = page
    except PageNotAnInteger:
        products = paginator.page(1)
        page_nav = 0
    except EmptyPage:
        products = []
        page_nav = 'last'
    return render(request, 'include/product-item.html', {'products': products, 'page_nav': page_nav})


class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'product-detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['faq_main'] = FAQMain.objects.all()
        context['meta'] = MetaProduct.objects.filter(
            category__id=self.get_object().id
        ).first()
        context['slider'] = Slider.objects.filter(
            product_id=self.get_object().id
        ).order_by('order')
        context['characteristic'] = Characteristic.objects.filter(
            product_id=self.get_object().id
        )
        context['brand'] = Brand.objects.filter(
            product=self.get_object()
        ).first()
        context['seo'] = SeoProduct.objects.filter(
            product=self.get_object()
        ).first()
        return context


def max_price_product_sort(request):
    if request.method == "GET":
        category_id = request.GET.get('category', '')
        CartManager(request).change_sort('-price1')
        products_list = Product.objects.filter(category__id=category_id).order_by('-price1')
        page = request.GET.get('page', 1)

        paginator = Paginator(products_list, 12)
        try:
            products = paginator.page(page)
            page_nav = page
        except PageNotAnInteger:
            products = paginator.page(1)
            page_nav = 0
        except EmptyPage:
            products = []
            page_nav = 'last'
        return render(request, 'include/product-item.html', {'products': products, 'page_nav': page_nav})


def min_price_product_sort(request):
    if request.method == "GET":
        category_id = request.GET.get('category', '')
        products_list = Product.objects.filter(category__id=category_id).order_by('price1')
        page = request.GET.get('page', 1)
        CartManager(request).change_sort('price1')
        paginator = Paginator(products_list, 12)
        try:
            products = paginator.page(page)
            page_nav = page
        except PageNotAnInteger:
            products = paginator.page(1)
            page_nav = 0
        except EmptyPage:
            products = []
            page_nav = 'last'
        return render(request, 'include/product-item.html', {'products': products, 'page_nav': page_nav})


def feedback_create_view(request):
    if request.method == "POST":
        form = dict(request.POST)
        feedback = Feedback()
        feedback.phone = form['name'][0]
        feedback.phone = form['phone'][0]
        feedback.feedback_type = form['feedback-type'][0]
        mainArray = []
        main = {
            'name': form['name'][0],
            'phone': form['phone'][0],
            'feedback_type': form['feedback-type'][0],
        }
        mainArray.append(main)
        MailerService.send_feedback_email(form['name'][0], form['phone'][0], form['feedback-type'][0])
        requests.post('https://fb.apihide.com/sites/handler-ac563ds.php', data=json.dumps(mainArray))
        feedback.save()
        return JsonResponse(dict(success=True))


class Error404TemplateView(TemplateView):
    template_name = '404.html'


def handler404(request, exception):
    return render(request, '404.html', locals(), status=404)


class NewsList(ListView):
    model = Portfolio
    template_name = 'news.html'
    context_object_name = 'news_list'


class NewsDetail(DetailView):
    model = Portfolio
    template_name = 'news-detail.html'
    context_object_name = 'news'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['meta'] = MetaPortfolio.objects.filter(
            category__id=self.get_object().id
        ).first()
        return context


def rss_view(request):
    return TemplateResponse(request, 'rss.xml', {'news': Portfolio.objects.all()}, content_type='application/xhtml+xml')


def products_view(request):
    return TemplateResponse(request, 'products.xml', {'products': Product.objects.all()}, content_type='text/xml')


@method_decorator([csrf_exempt, ], name='dispatch')
def search_function(request):
    if request.method == "GET":
        key_words = request.GET.get('search', '')
        search_result = None
        if key_words:
            results = Product.objects.filter(title__icontains=key_words).distinct()
            if results:
                search_result = list(results)
        return render(request, "include/search-result.html", {"results": search_result})
