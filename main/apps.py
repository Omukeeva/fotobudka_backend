from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'
    verbose_name = "2. Инфо для главной и сквозные блоки"
