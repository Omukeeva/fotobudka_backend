document.addEventListener('DOMContentLoaded', () => {
    let observer = lozad('.lozad');
    observer.observe();


    new Swiper('.portfolio-detail-slider', {
        spaceBetween: 16,
        pagination: {
            el: '.portfolio-detail-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.portfolio-detail-next',
            prevEl: '.portfolio-detail-prev',
        },
    });

    new Swiper('.product-detail-slider', {
        spaceBetween: 20,
        pagination: {
            el: '.product-detail-pagination',
            clickable: true,
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            960: {
                slidesPerView: 2,
            }
        },
        navigation: {
            nextEl: '.product-detail-next',
            prevEl: '.product-detail-prev',
        },
    });
    new Swiper('.product-brand-slider', {
        spaceBetween: 20,
        pagination: {
            el: '.product-brand-pagination',
            clickable: true,
        },
        lazy: true,
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            640: {
                slidesPerView: 2,
            },
            960: {
                slidesPerView: 3,
            }
        },
        navigation: {
            nextEl: '.product-brand-next',
            prevEl: '.product-brand-prev',
        },
    });
    new Swiper('.options-slider', {
        spaceBetween: 20,
        slidesPerView: 'auto',
        breakpoints: {
            320: {
                spaceBetween: 8,
            },
            960: {
                spaceBetween: 20,
            }
        },
        navigation: {
            nextEl: '.options-next',
            prevEl: '.options-prev',
        },
    });

    new Swiper('.recommendation-slider', {
        spaceBetween: 20,
        slidesPerView: 3,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 8,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            960: {
                spaceBetween: 20,
                slidesPerView: 3,
            }
        },
        navigation: {
            nextEl: '.recommendation-next',
            prevEl: '.recommendation-prev',
        }
    });
    if (window.innerWidth <= 640) {
        new Swiper('.main-info-slider', {
            loop: true,
            spaceBetween: 16,
            pagination: {
                el: '.main-info-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.main-info-next',
                prevEl: '.main-info-prev',
            }
        });
    }
    if (window.innerWidth <= 960) {
        new Swiper('.event-example-slider', {
            spaceBetween: 16,
            pagination: {
                el: '.event-example-pagination',
                clickable: true,
            }
        });
        document.querySelector('.burger').addEventListener('click', function () {
            document.querySelector('.burger').classList.toggle('show');
            document.querySelector('.mobile-menu').classList.toggle('show');
        });
        document.querySelector('.catalog-show').addEventListener('click', function () {
            document.querySelector('.catalog-show').classList.toggle('active');
        });
        document.querySelector('.events-show').addEventListener('click', function () {
            document.querySelector('.events-show').classList.toggle('active');
        });
    }
    if (window.innerWidth <= 960) {
        let promotionItem = document.querySelectorAll('.promotion-item');
        promotionItem.forEach(function (el, i) {
            promotionItem[i].addEventListener("click", function () {
                if (promotionItem[i].classList.contains('active')) {
                    promotionItem[i].classList.remove("active");
                } else {
                    promotionItem[i].closest('.promotion-list').querySelectorAll('.promotion-item').forEach(function (elm, index) {
                        promotionItem[i].closest('.promotion-list').querySelectorAll('.promotion-item')[index].classList.remove('active');
                    });
                    promotionItem[i].classList.add("active");
                }
            });
        });
    }

    new Swiper('.news-slider', {
        slidesPerView: 3,
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            640: {
                slidesPerView: 2,
            },
            960: {
                slidesPerView: 3,
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

});

function addSlicePrice(number) {
    let str = number.toString();
    if (str.length > 4) {
        return str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
    } else {
        return str
    }
}

if (document.querySelector('.form-product-price')) {
    document.querySelector('.form-product-price').textContent = addSlicePrice(document.querySelector('.form-product-price').textContent);
}
if (document.querySelectorAll('.basket-item-price')) {
    document.querySelectorAll('.basket-item-price').forEach(function (el, i) {
        document.querySelectorAll('.basket-item-price')[i].textContent = addSlicePrice(document.querySelectorAll('.basket-item-price')[i].textContent)
    });
    document.querySelectorAll('.basket-total-price').forEach(function (el, i) {
        document.querySelectorAll('.basket-total-price')[i].textContent = addSlicePrice(document.querySelectorAll('.basket-total-price')[i].textContent)
    })
}
if (document.querySelectorAll('.option-price')) {
    document.querySelectorAll('.option-price').forEach(function (el, i) {
        document.querySelectorAll('.option-price')[i].textContent = addSlicePrice(document.querySelectorAll('.option-price')[i].textContent)
    })
}

function removeSlice(number) {
    let str = number.toString();
    return str.split(' ').join('')
}

function scrollToFunction(selector, yOffset = 68) {
    const el = document.querySelector('#' + selector);
    const y = el.getBoundingClientRect().top + window.pageYOffset - yOffset;
    window.scrollTo({top: y, behavior: 'smooth'});
}


if (document.querySelector('.catalog-btn')) {
    document.querySelector('.catalog-btn').addEventListener('click', function () {
        scrollToFunction('catalog');
    })
}

if (document.querySelector('.products-btn')) {
    document.querySelector('.products-btn').addEventListener('click', function () {
        scrollToFunction('products')
    })
}
if (document.querySelectorAll('.contacts-btn')) {
    document.querySelectorAll('.contacts-btn').forEach(function (el, i) {
        document.querySelectorAll('.contacts-btn')[i].addEventListener('click', function () {
            scrollToFunction('contacts');
            document.querySelector('.mobile-menu').classList.remove('show');
            document.querySelector('.burger').classList.remove('show');
        })
    })
}
if (!document.querySelector('#faq')) {
    document.querySelector('.faq-btn').style.display = 'none'
}
if (document.querySelectorAll('.faq-btn')) {
    document.querySelectorAll('.faq-btn').forEach(function (el, i) {
        document.querySelectorAll('.faq-btn')[i].addEventListener('click', function () {
            scrollToFunction('faq');
            document.querySelector('.mobile-menu').classList.remove('show');
            document.querySelector('.burger').classList.remove('show');
        })
    })
}


let accTitle = document.querySelectorAll(".accordion-item");

accTitle.forEach(function (el, i) {
    accTitle[i].addEventListener("click", function () {
        if (accTitle[i].classList.contains('active')) {
            accTitle[i].classList.remove("active");
        } else {
            accTitle[i].closest('.accordion-list').querySelectorAll('.accordion-item').forEach(function (elm, index) {
                accTitle[i].closest('.accordion-list').querySelectorAll('.accordion-item')[index].classList.remove('active');
            });
            accTitle[i].classList.add("active");
        }
    });
});


if (document.querySelector('.preview')) {
    document.querySelector('.preview').addEventListener('click', function () {
        document.querySelector('.preview').style.display = 'none';
        document.querySelector('.preview').closest('.video').querySelector('iframe').style.display = 'block';
        document.querySelector('.preview').closest('.video').querySelector('iframe').setAttribute('src', document.querySelector('.preview').closest('.video').querySelector('iframe').getAttribute('data-src'));
    });
}
initSlider();

function initSlider() {
    new Swiper('.catalog-item-slider', {
        spaceBetween: 16,
        lazy: true,
        loadPrevNextAmount: 2,
        pagination: {
            el: '.catalog-item-pagination',
        }
    });


    let productItem = document.querySelectorAll('.catalog-item');
    productItem.forEach(function (el, i) {
        productItem[i].querySelectorAll('.swiper-pagination-bullet').forEach(function (element, index) {
            productItem[i].querySelectorAll('.swiper-pagination-bullet')[index].addEventListener('mouseover', function () {
                productItem[i].querySelector('.catalog-item-slider').swiper.slideTo(index);
            });
        });
        productItem[i].querySelector('.catalog-item-price-js').textContent = addSlicePrice(productItem[i].querySelector('.catalog-item-price-js').textContent);
        productItem[i].querySelector('.catalog-item-slider').addEventListener('click', function () {
            window.location.href = productItem[i].querySelector('.catalog-item-link').getAttribute('href');
        });
    });
    // ####################### FAVORITES BLOCK ################################


    let favorite = document.querySelectorAll('.favorite-js');
    if (favorite) {
        favorite.forEach(function (el, i) {
            favorite[i].addEventListener('click', function () {
                let product_id = favorite[i].getAttribute('data-id');
                let request = new XMLHttpRequest();
                request.open("POST", '/favorite/', true);
                request.setRequestHeader("Content-Type", "application/json");
                let data = {
                    product_id: product_id,
                };
                request.send(JSON.stringify(data));

                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        if (request.status === 200 && request.status < 300) {
                            let response = JSON.parse(request.response);
                            if (response.message === 'Removed') {
                                favorite[i].classList.remove('active');
                                if (favorite[i].closest('.favorite-list')) {
                                    favorite[i].closest('.catalog-item').style.display = 'none';
                                }
                            } else {
                                favorite[i].classList.add('active');
                                document.querySelector('.favorite-count').classList.add('active');
                            }
                            if (response.count_favorite !== 0) {
                                document.querySelector('.favorite-count').innerHTML = response.count_favorite;
                            } else {
                                document.querySelector('.favorite-count').classList.remove('active');
                                document.querySelector('.favorite-count').innerHTML = '';
                            }
                        } else {
                            alert('Что то пошло не так... перезагрузите страницу')
                        }
                    }
                }
            });
        });
    }

// ####################### FAVORITES BLOCK ################################
    let mainVideo = document.querySelector('#video');
    if (document.querySelectorAll('.show-video')) {
        document.querySelectorAll('.show-video').forEach(function (el, i) {
            document.querySelectorAll('.show-video')[i].addEventListener('click', () => {
                mainVideo.setAttribute('src', document.querySelectorAll('.show-video')[i].getAttribute('data-src'));
                document.querySelector('.modal-video').classList.add('show');
            });
        })
    }

    let modal = document.querySelectorAll('.modal');
    modal.forEach(function (el, index) {
        modal[index].addEventListener('click', function (event) {
            if (!event.target.closest('.modal-content')) {
                if (modal[index].classList.contains('show')) {
                    modal[index].classList.remove('show');
                    if (modal[index].querySelector('iframe')) {
                        modal[index].querySelector('iframe').setAttribute('src', '')
                    }
                }
                if (mainVideo) {
                    mainVideo.setAttribute('src', '');
                }
            }
        });
    });
}


let mainImage = document.querySelector('#image');
if (document.querySelectorAll('.show-image')) {
    document.querySelectorAll('.show-image').forEach(function (el, i) {
        document.querySelectorAll('.show-image')[i].addEventListener('click', () => {
            document.querySelector('.modal-image').classList.add('show');
            mainImage.setAttribute('src', document.querySelectorAll('.show-image')[i].getAttribute('data-src'));
        });
    })
}
document.querySelectorAll('.modal-close').forEach(function (el, i) {
    document.querySelectorAll('.modal-close')[i].addEventListener('click', function () {
        document.querySelectorAll('.modal-close')[i].closest('.modal').classList.remove('show');
        if (document.querySelectorAll('.modal-close')[i].closest('.modal').querySelector('#video')) {
            document.querySelector('#video').setAttribute('src', '');
        }
    })
});
if (document.querySelector('.feedback-form-btn')) {
    document.querySelector('.feedback-form-btn').addEventListener('click', function () {
        document.querySelector('.feedback-modal').classList.add('show');
        inputValidate();
    });
}

document.querySelectorAll('.catalog-sort-default').forEach(function (el, i) {
    document.querySelectorAll('.catalog-sort-default')[i].addEventListener('click', function () {
        document.querySelector('.catalog-sort-fixed').classList.toggle('show');
    })
});
document.querySelectorAll('.catalog-filter-default').forEach(function (el, i) {
    document.querySelectorAll('.catalog-filter-default')[i].addEventListener('click', function () {
        document.querySelector('.catalog-filters-fixed').classList.toggle('show');
    })
});
document.querySelectorAll('.catalog-modal-close').forEach(function (el, i) {
    document.querySelectorAll('.catalog-modal-close')[i].addEventListener('click', function () {
        document.querySelectorAll('.catalog-modal-close')[i].closest('.catalog-modal-fixed ').classList.remove('show');
    })
});
document.querySelectorAll('.feedback-type-check').forEach(function (el, i) {
    document.querySelectorAll('.feedback-type-check')[i].addEventListener('click', function () {
        document.querySelectorAll('.feedback-type-check')[i].closest('.feedback-type').querySelector('.feedback-type-list').classList.toggle('show');
    })
});

let feedbackItem = document.querySelectorAll('.feedback-type-item');
feedbackItem.forEach(function (el, index) {
    feedbackItem[index].addEventListener('click', () => {
        let feedbackType = feedbackItem[index].textContent;
        feedbackItem[index].closest('.feedback-type-list').classList.remove('show');
        feedbackItem[index].closest('.feedback-type').querySelector('.feedback-type-input').value = feedbackType;
        feedbackItem[index].closest('.feedback-type').querySelector('.feedback-type-check').textContent = feedbackType;
    });
});

// ####################### PRODUCTS SORT ###############################

document.querySelectorAll('.max-price').forEach(function (el, i) {
    document.querySelectorAll('.max-price')[i].addEventListener('click', (e) => {
        e.preventDefault();
        let request = new XMLHttpRequest();
        let catId = document.querySelector('.product-section').getAttribute('data-category');
        request.open('GET', '/sort_max_price/?category=' + catId, true);
        request.send();

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200 && request.status < 300) {
                    document.querySelector(".product-cart-block").innerHTML = request.response;
                    if (window.location.search || document.querySelector('.page-first').getAttribute('data-page') === '1') {
                        let page = '1';
                        if (window.location.search) {
                            page = window.location.search.split('=')[1]
                        }
                        document.querySelectorAll('.catalog-page').forEach(function (el, i) {
                            document.querySelectorAll('.catalog-page')[i].classList.remove('active');
                            if (document.querySelectorAll('.catalog-page')[i].classList.contains(page)) {
                                document.querySelectorAll('.catalog-page')[i].classList.add('active')
                            }
                        });
                        document.querySelector('.catalog-next').classList.remove('disabled');
                        if (document.querySelector('.show-more')) {
                            document.querySelector('.show-more').style.display = 'flex';
                            document.querySelector('.show-more').setAttribute('data-count', parseInt(page) + 1);
                        }
                        if (document.querySelector('.catalog-prev')) {
                            if (page >= 2) {
                                document.querySelector('.catalog-prev').classList.remove('disabled');
                                document.querySelector('.catalog-prev').setAttribute('href', '?page=' + (parseInt(page) - 1));
                            } else {
                                document.querySelector('.catalog-prev').classList.add('disabled');
                            }
                        }
                        if (document.querySelector('.page-last').getAttribute('data-page') === page) {
                            document.querySelector('.catalog-next').classList.add('disabled');
                            if (document.querySelector('.show-more')) {
                                document.querySelector('.show-more').style.display = 'none';
                            }
                        }
                    }
                    initSlider();
                } else {
                    alert('Что то пошло не так, перезагрузите страницу')
                }
            }
        };
        document.querySelectorAll('.min-price')[i].classList.remove('active');
        document.querySelectorAll('.max-price')[i].classList.add('active');
        window.scrollTo({
            top: window.pageYOffset + document.querySelector('.product-cart-block').getBoundingClientRect().top - 200,
            behavior: 'smooth'
        });
    });
});

document.querySelectorAll('.min-price').forEach(function (el, i) {
    document.querySelectorAll('.min-price')[i].addEventListener('click', (e) => {
        e.preventDefault();
        let request = new XMLHttpRequest();
        let catId = document.querySelector('.product-section').getAttribute('data-category');
        request.open('GET', '/sort_min_price/?category=' + catId, true);
        request.send();

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200 && request.status < 300) {
                    document.querySelector(".product-cart-block").innerHTML = request.response;
                    if (window.location.search || document.querySelector('.page-first').getAttribute('data-page') === '1') {
                        let page = '1';
                        if (window.location.search) {
                            page = window.location.search.split('=')[1]
                        }
                        document.querySelectorAll('.catalog-page').forEach(function (el, i) {
                            document.querySelectorAll('.catalog-page')[i].classList.remove('active');
                            if (document.querySelectorAll('.catalog-page')[i].classList.contains(page)) {
                                document.querySelectorAll('.catalog-page')[i].classList.add('active')
                            }
                        });
                        document.querySelector('.catalog-next').classList.remove('disabled');
                        if (document.querySelector('.show-more')) {
                            document.querySelector('.show-more').style.display = 'flex';
                            document.querySelector('.show-more').setAttribute('data-count', parseInt(page) + 1);
                        }
                        if (document.querySelector('.catalog-prev')) {
                            if (page >= 2) {
                                document.querySelector('.catalog-prev').classList.remove('disabled');
                                document.querySelector('.catalog-prev').setAttribute('href', '?page=' + (parseInt(page) - 1));
                            } else {
                                document.querySelector('.catalog-prev').classList.add('disabled');
                            }
                        }
                        if (document.querySelector('.page-last').getAttribute('data-page') === page) {
                            document.querySelector('.catalog-next').classList.add('disabled');
                            if (document.querySelector('.show-more')) {
                                document.querySelector('.show-more').style.display = 'none';
                            }
                        }
                    }
                    initSlider();
                } else {
                    alert('Что то пошло не так, перезагрузите страницу')
                }
            }
        };
        document.querySelectorAll('.max-price')[i].classList.remove('active');
        document.querySelectorAll('.min-price')[i].classList.add('active');
        window.scrollTo({
            top: window.pageYOffset + document.querySelector('.product-cart-block').getBoundingClientRect().top - 200,
            behavior: 'smooth'
        });
    });
});


// ####################### PRODUCTS SORT ###############################

// ####################### OPTIONS SORT ###############################

let optionSortItem = document.querySelectorAll('.options-sort-item');

optionSortItem.forEach(function (el, i) {
    optionSortItem[i].addEventListener('click', function () {
        optionSortItem[i].closest('.options-sort').querySelectorAll('.options-sort-item').forEach(function (el, i) {
            optionSortItem[i].closest('.options-sort').querySelectorAll('.options-sort-item')[i].classList.remove('active')
        });
        optionSortItem[i].classList.add('active');
        if (optionSortItem[i].classList.contains('active')) {
            let optionCategoryId = optionSortItem[i].getAttribute('data-id');
            let optionItem = document.querySelectorAll('.option-item');
            optionItem.forEach(function (el, item) {
                let optionCategory = optionItem[item].getAttribute('data-category-id');
                if (optionCategory === optionCategoryId) {
                    optionItem[item].style.display = 'block'
                } else {
                    optionItem[item].style.display = 'none'
                }
                if (optionCategoryId === 'all') {
                    optionItem[item].style.display = 'block'
                }
            });
        }
        document.getElementsByClassName('options-slider')[0].swiper.update()
    })
});


// ####################### OPTIONS SORT ###############################


// ####################### TIME SIZE SCRIPT ################################
let optionTotalPrice = 0;
let productTotalPrice = 0;
if (document.querySelector('.form-product-price')) {
    productTotalPrice = parseInt(document.querySelector('.form-product-price').getAttribute('data-price'));
}
if (document.querySelectorAll('.option-item')) {
    document.querySelectorAll('.option-item').forEach(function (el, i) {
        if (document.querySelectorAll('.option-item')[i].classList.contains('active')) {
            optionTotalPrice += parseInt(document.querySelectorAll('.option-item')[i].querySelector('.option-price').getAttribute('data-price'))
        }
    })
}

let timeItem = document.querySelectorAll('.time-item');
let sizeItem = document.querySelectorAll('.size-item');
sizeItem.forEach(function (el, i) {
    sizeItem[i].addEventListener('click', function () {
        sizeItem[i].closest('.size').querySelectorAll('.size-item').forEach(function (elem, index) {
            sizeItem[i].closest('.size').querySelectorAll('.size-item')[index].classList.remove('active')
        });
        let price = 0;
        timeItem.forEach(function (el, i) {
            if (timeItem[i].classList.contains('active')) {
                price = timeItem[i].querySelector('input').value
            }
        });
        sizeItem[i].classList.add('active');
        if (sizeItem[i].classList.contains('active')) {
            productTotalPrice = parseInt(sizeItem[i].querySelector('input').value) + parseInt(price);
            let total = optionTotalPrice + productTotalPrice;
            document.querySelector('.form-product-price').textContent = addSlicePrice(total);
            document.querySelector('.form-product-price').setAttribute('data-total-price', total)
        }

        if (document.querySelector('.add-cart').classList.contains('active')) {
            let productId = sizeItem[i].closest('.product').getAttribute('data-id');
            let size = sizeItem[i].querySelector('span').textContent;
            let size_price = sizeItem[i].querySelector('input').value;
            productSizeUpdateFunction(productId, size, size_price)
        }
    })
});

timeItem.forEach(function (el, i) {
    timeItem[i].addEventListener('click', function () {
        timeItem[i].closest('.time').querySelectorAll('.time-item').forEach(function (elem, index) {
            timeItem[i].closest('.time').querySelectorAll('.time-item')[index].classList.remove('active')
        });
        timeItem[i].classList.add('active');
        let price = 0;
        sizeItem.forEach(function (el, i) {
            if (sizeItem[i].classList.contains('active')) {
                price = sizeItem[i].querySelector('input').value
            }
        });
        if (timeItem[i].classList.contains('active')) {
            productTotalPrice = parseInt(timeItem[i].querySelector('input').value) + parseInt(price);
            let total = optionTotalPrice + productTotalPrice;
            document.querySelector('.form-product-price').textContent = addSlicePrice(total);
            document.querySelector('.form-product-price').setAttribute('data-price', timeItem[i].querySelector('input').value);
            document.querySelector('.form-product-price').setAttribute('data-total-price', total);
        }
        if (document.querySelector('.add-cart').classList.contains('active')) {
            let productId = timeItem[i].closest('.product').getAttribute('data-id');
            let time = timeItem[i].querySelector('input').getAttribute('data-time');
            let time_price = timeItem[i].querySelector('input').value;
            productTimeUpdateFunction(productId, time, time_price)
        }
    });
});


// ####################### TIME SCRIPT ################################


// ####################### OPTION SCRIPT ################################
let optionContent = document.querySelectorAll('.option-click');
optionContent.forEach(function (el, i) {
    let optionInput = optionContent[i].closest('.option-item').querySelector('.option-input');
    if (optionInput.checked) {
        optionTotalPrice += parseInt(optionInput.closest('.option-item').querySelector('.option-price').getAttribute('data-price'));
    }
    optionContent[i].addEventListener('click', function () {
        optionContent[i].classList.remove('active');
        optionInput.closest('.option-item').classList.add('active');
        optionInput.closest('.option-item').querySelector('.option-item-quantity').classList.add('active');
        optionInput.checked = true;
        optionTotalPrice += parseInt(optionInput.closest('.option-item').querySelector('.option-price').getAttribute('data-price'));
        document.querySelector('.form-product-price').textContent = addSlicePrice(optionTotalPrice + productTotalPrice);
        document.querySelector('.form-product-price').setAttribute('data-total-price', optionTotalPrice + productTotalPrice);
        let optionId = optionContent[i].closest('.option-item').getAttribute('data-id');
        let optionQuantity = optionContent[i].closest('.option-item').querySelector('.option-quantity-input').value;
        let optionPrice = removeSlice(optionContent[i].closest('.option-item').querySelector('.option-price').textContent);
        optionListCreate(optionId, optionQuantity, optionPrice);
        if (document.querySelector('.add-cart').classList.contains('active')) {
            optionAddFunction(optionContent[i].closest('.options').getAttribute('data-product-id'));
        } else {
            // ajaxRequest()
        }
    })
});
// ####################### OPTION SCRIPT ################################

// ####################### PLUS/MINUS SCRIPT ################################
let options = [];

let minus = document.querySelectorAll('.minus-js');
minus.forEach(function (el, i) {
    minus[i].addEventListener('click', function () {
        let val = minus[i].closest('.default-quantity').querySelector('input');
        if (val.value === '1') {
            val.value = 1;
            if (minus[i].closest('.option-item')) {
                minus[i].closest('.option-item').querySelector('.option-click').classList.add('active');
                minus[i].closest('.option-item').classList.remove('active');
                minus[i].closest('.option-item').querySelector('.option-item-quantity').classList.remove('active');
                minus[i].closest('.option-item').querySelector('.option-input').checked = false;
                let optionId = minus[i].closest('.option-item').getAttribute('data-id');
                optionTotalPrice -= parseInt(minus[i].closest('.option-item').querySelector('.option-price').getAttribute('data-price'));
                minus[i].closest('.option-item').querySelector('.option-price').textContent = optionContent[i].closest('.option-item').querySelector('.option-price').getAttribute('data-first-price');
                minus[i].closest('.option-item').querySelector('.option-price').setAttribute('data-price', optionContent[i].closest('.option-item').querySelector('.option-price').getAttribute('data-first-price'));
                for (let k of options) {
                    if (k.id === optionId) {
                        let index = options.indexOf(k);
                        if (index !== -1) {
                            options.splice(index, 1);
                        }
                    }
                }
                if (document.querySelector('.add-cart').classList.contains('active')) {
                    optionRemoveFunction(document.querySelector('.product').getAttribute('data-id'), optionId);
                }
            }
        } else {
            let minusVal = val.value;
            minusVal--;
            val.value = minusVal;

            if (minus[i].closest('.option-item')) {
                let optionPrice = minus[i].closest('.option-item').querySelector('.option-price').getAttribute('data-first-price');
                minus[i].closest('.option-item').querySelector('.option-price').setAttribute('data-price', optionPrice * val.value);
                minus[i].closest('.option-item').querySelector('.option-price').textContent = addSlicePrice(optionPrice * val.value);
                optionTotalPrice -= parseInt(optionPrice);
                let productId = minus[i].closest('.options').getAttribute('data-product-id');
                let optionId = minus[i].closest('.option-item').getAttribute('data-id');
                let optionQuantity = minus[i].closest('.option-item').querySelector('.option-quantity-input').value;
                let optionItemPrice = optionPrice * val.value;
                optionUpdateFunction(productId, optionId, optionQuantity, optionItemPrice);
            }

            if (minus[i].closest('.option-basket-item')) {
                let optionPrice = minus[i].closest('.option-basket-item').querySelector('.basket-item-block-price').getAttribute('data-price');
                minus[i].closest('.option-basket-item').querySelector('.basket-item-price').textContent = addSlicePrice(optionPrice * val.value);
                let productId = minus[i].closest('.option-basket-item').getAttribute('data-product-id');
                let optionId = minus[i].closest('.option-basket-item').getAttribute('data-option-id');
                let optionQuantity = minus[i].closest('.option-basket-item').querySelector('.option-basket-input').value;
                let optionItemPrice = optionPrice * val.value;
                optionUpdateFunction(productId, optionId, optionQuantity, optionItemPrice);
            }
        }
        if (document.querySelector('.form-product-price')) {
            document.querySelector('.form-product-price').textContent = addSlicePrice(optionTotalPrice + productTotalPrice);
            document.querySelector('.form-product-price').setAttribute('data-total-price', optionTotalPrice + productTotalPrice);
        }
    })
});

let plus = document.querySelectorAll('.plus-js');
plus.forEach(function (el, i) {
    plus[i].addEventListener('click', function () {
        let val = plus[i].closest('.default-quantity').querySelector('input');
        let plusVal = val.value;
        plusVal++;
        val.value = plusVal;
        if (plus[i].closest('.option-item')) {
            let optionPrice = plus[i].closest('.option-item').querySelector('.option-price').getAttribute('data-first-price');
            plus[i].closest('.option-item').querySelector('.option-price').setAttribute('data-price', optionPrice * val.value);
            plus[i].closest('.option-item').querySelector('.option-price').textContent = addSlicePrice(optionPrice * val.value);
            optionTotalPrice += parseInt(optionPrice);
            let productId = plus[i].closest('.options').getAttribute('data-product-id');
            let optionId = plus[i].closest('.option-item').getAttribute('data-id');
            let optionQuantity = plus[i].closest('.option-item').querySelector('.option-quantity-input').value;
            let optionItemPrice = optionPrice * val.value;
            optionUpdateFunction(productId, optionId, optionQuantity, optionItemPrice);
        }
        if (plus[i].closest('.option-basket-item')) {
            let optionPrice = plus[i].closest('.option-basket-item').querySelector('.basket-item-block-price').getAttribute('data-price');
            plus[i].closest('.option-basket-item').querySelector('.basket-item-price').textContent = addSlicePrice(optionPrice * val.value);
            let productId = plus[i].closest('.option-basket-item').getAttribute('data-product-id');
            let optionId = plus[i].closest('.option-basket-item').getAttribute('data-option-id');
            let optionQuantity = plus[i].closest('.option-basket-item').querySelector('.option-basket-input').value;
            let optionItemPrice = optionPrice * val.value;
            optionUpdateFunction(productId, optionId, optionQuantity, optionItemPrice);
        }
        if (document.querySelector('.form-product-price')) {
            document.querySelector('.form-product-price').textContent = addSlicePrice(optionTotalPrice + productTotalPrice);
            document.querySelector('.form-product-price').setAttribute('data-total-price', optionTotalPrice + productTotalPrice);
        }
    })
});

// ####################### PLUS/MINUS SCRIPT ################################

function optionListCreate(optionId, optionQuantity, optionPrice) {
    if (options.length === 0) {
        options.push({
            id: optionId,
            quantity: optionQuantity,
            price: optionPrice
        })
    } else {
        let isEmpty = false;
        for (let k of options) {
            if (k.id === optionId) {
                isEmpty = true;
            }
        }
        if (!isEmpty) {
            options.push({
                id: optionId,
                quantity: optionQuantity,
                price: optionPrice
            })
        } else {
            for (let i of options) {
                if (i.id === optionId) {
                    i.quantity = optionQuantity;
                    i.price = optionPrice
                }
            }
        }
    }
}

// ####################### ADD CART SCRIPT ################################
if (document.querySelector('.add-cart')) {
    document.querySelector('.add-cart').addEventListener('click', function () {
        if (document.querySelector('.add-cart').classList.contains('active')) {
            window.location.href = '/basket'
            // ajaxRequestDeleteProduct(document.querySelector('.product').getAttribute('data-id'))
        } else {
            ajaxRequest();
        }
    })
}

let optionsItem = document.querySelectorAll('.option-item');
optionsItem.forEach(function (el, i) {
    if (optionsItem[i].classList.contains('active')) {
        let optionId = optionsItem[i].getAttribute('data-id');
        let optionQuantity = optionsItem[i].querySelector('.option-quantity-input').value;
        let optionPrice = removeSlice(optionsItem[i].querySelector('.option-price').textContent);
        optionListCreate(optionId, optionQuantity, optionPrice);
    }
});

function ajaxRequest() {
    let timeProduct = document.querySelectorAll('.time-item');
    let timeProductValue = '';
    let timeProductPrice = '';
    if (timeProduct) {
        timeProduct.forEach(function (el, i) {
            if (timeProduct[i].classList.contains('active')) {
                timeProductValue = timeProduct[i].querySelector('input').getAttribute('data-time');
                timeProductPrice = timeProduct[i].querySelector('input').value
            }
        })
    }
    let sizeProduct = document.querySelectorAll('.size-item');
    let sizeProductValue = '';
    let sizeProductPrice = '';
    if (sizeProduct) {
        sizeProduct.forEach(function (el, i) {
            if (sizeProduct[i].classList.contains('active')) {
                sizeProductValue = sizeProduct[i].querySelector('span').textContent;
                sizeProductPrice = sizeProduct[i].querySelector('input').value
            }
        })
    }


    let totalProductPrice = document.querySelector('.price-js').getAttribute('data-total-price');
    let product_id = document.querySelector('.product').getAttribute('data-id');
    let quantity = '1';
    let request = new XMLHttpRequest();
    request.open("POST", '/add_cart/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
        quantity: quantity,
        price: totalProductPrice,
        time: timeProductValue,
        time_price: timeProductPrice,
        size: sizeProductValue,
        size_price: sizeProductPrice,
        options: options,
    };
    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
                document.querySelector('.basket-count').classList.add('active');
                document.querySelector('.basket-count').textContent = response.cart_quantity;
                if (document.querySelector('.add-cart')) {
                    document.querySelector('.add-cart').classList.add('active');
                    document.querySelector('.add-cart').textContent = 'Перейти в корзину';
                }
            }
        }
    }
}

// ####################### ADD CART SCRIPT ################################

// ####################### REMOVE CART SCRIPT ################################


function ajaxRequestDeleteProduct(product_id) {
    let request = new XMLHttpRequest();
    request.open("POST", '/remove_cart/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
    };

    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
                optionTotalPrice = 0;
                options = [];
                if (document.querySelector('.add-cart')) {
                    document.querySelector('.add-cart').classList.remove('active');
                    document.querySelector('.add-cart').textContent = 'Добавить';
                    document.querySelector('.form-product-price').textContent = addSlicePrice(document.querySelector('.form-product-price').getAttribute('data-price'));
                    document.querySelectorAll('.option-item').forEach(function (element, item) {
                        if (document.querySelectorAll('.option-item')[item].classList.contains('active')) {
                            document.querySelectorAll('.option-item')[item].classList.remove('active');
                            document.querySelectorAll('.option-item')[item].querySelector('.option-click').classList.add('active');
                            document.querySelectorAll('.option-item')[item].querySelector('.option-item-quantity').classList.remove('active');
                            document.querySelectorAll('.option-item')[item].querySelector('.option-input').checked = false;
                            document.querySelectorAll('.option-item')[item].querySelector('.option-quantity-input').value = '1';
                            document.querySelectorAll('.option-item')[item].querySelector('.option-price').textContent = addSlicePrice(document.querySelectorAll('.option-item')[item].querySelector('.option-price').getAttribute('data-first-price'));
                            document.querySelectorAll('.option-item')[item].querySelector('.option-price').setAttribute('data-price', document.querySelectorAll('.option-item')[item].querySelector('.option-price').getAttribute('data-first-price'));
                        }
                    });
                }

                if (response.cart_quantity === 0) {
                    document.querySelector('.basket-count').classList.remove('active');
                    document.querySelector('.basket-count').textContent = ''
                } else {
                    document.querySelector('.basket-count').textContent = response.cart_quantity;
                }
                if (document.querySelector('.basket-desktop-price')) {
                    document.querySelector('.basket-desktop-price').textContent = addSlicePrice(response.total_price);
                    document.querySelector('.basket-mobile-price').textContent = addSlicePrice(response.total_price);
                }
            }
        }
    }
}

document.querySelectorAll('.product-delete').forEach(function (el, i) {
    document.querySelectorAll('.product-delete')[i].addEventListener('click', function () {
        ajaxRequestDeleteProduct(document.querySelectorAll('.product-delete')[i].closest('.product-basket-item').getAttribute('data-product-id'));
        document.querySelectorAll('.product-delete')[i].closest('.basket-parent').style.display = 'none';
    })
});

// ####################### REMOVE CART SCRIPT ################################

// ####################### REMOVE OPTION SCRIPT ################################

document.querySelectorAll('.option-delete').forEach(function (el, i) {
    document.querySelectorAll('.option-delete')[i].addEventListener('click', function () {
        let product_id = document.querySelectorAll('.option-delete')[i].closest('.option-basket-item').getAttribute('data-product-id');
        let option_id = document.querySelectorAll('.option-delete')[i].closest('.option-basket-item').getAttribute('data-option-id');
        document.querySelectorAll('.option-delete')[i].closest('.option-basket-item').classList.add('removed');
        optionRemoveFunction(product_id, option_id);
        document.querySelectorAll('.option-delete')[i].closest('.option-basket-item').style.display = 'none';
        let optionBasketItem = document.querySelectorAll('.option-delete')[i].closest('.basket-option-list').querySelectorAll('.option-basket-item');
        let removed = 0;
        optionBasketItem.forEach(function (elem, index) {
            if (optionBasketItem[index].classList.contains('removed')) {
                removed += 1
            }
        });
        if (removed === optionBasketItem.length) {
            document.querySelectorAll('.option-delete')[i].closest('.basket-option-list').querySelector('.basket-option-title').style.display = 'none';
        }
    })
});

function optionRemoveFunction(product_id, option_id) {
    let request = new XMLHttpRequest();
    request.open("POST", '/remove_option_cart/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
        option_id: option_id,
    };

    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
                if (document.querySelector('.basket-desktop-price')) {
                    document.querySelector('.basket-desktop-price').textContent = addSlicePrice(response.total_price);
                    document.querySelector('.basket-mobile-price').textContent = addSlicePrice(response.total_price);
                }
            }
        }
    }
}

// ####################### REMOVE OPTION SCRIPT ################################

// ####################### UPDATE SCRIPT ################################

function productTimeUpdateFunction(product_id, time, time_price) {
    let request = new XMLHttpRequest();
    request.open("POST", '/product_time_update/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
        time: time,
        time_price: time_price,
    };

    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
                if (document.querySelector('.basket-desktop-price')) {
                    document.querySelector('.basket-desktop-price').textContent = addSlicePrice(response.total_price);
                    document.querySelector('.basket-mobile-price').textContent = addSlicePrice(response.total_price);
                }
                if (document.querySelector('.basket')) {
                    document.querySelectorAll('.product-basket-item').forEach(function (el, i) {
                        if (document.querySelectorAll('.product-basket-item')[i].getAttribute('data-product-id') === product_id) {
                            document.querySelectorAll('.product-basket-item')[i].querySelector('.basket-item-price').textContent = addSlicePrice(response.price)
                        }
                    })
                }
            }
        }
    }
}

function productSizeUpdateFunction(product_id, size, size_price) {
    let request = new XMLHttpRequest();
    request.open("POST", '/product_size_update/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
        size: size,
        size_price: size_price,
    };

    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
                if (document.querySelector('.basket-desktop-price')) {
                    document.querySelector('.basket-desktop-price').textContent = addSlicePrice(response.total_price);
                    document.querySelector('.basket-mobile-price').textContent = addSlicePrice(response.total_price);
                }

                if (document.querySelector('.basket')) {
                    document.querySelectorAll('.product-basket-item').forEach(function (el, i) {
                        if (document.querySelectorAll('.product-basket-item')[i].getAttribute('data-product-id') === product_id) {
                            document.querySelectorAll('.product-basket-item')[i].querySelector('.basket-item-price').textContent = addSlicePrice(response.price)
                        }
                    })
                }
            }
        }
    }
}

function optionUpdateFunction(product_id, option_id, option_quantity, option_price) {
    let request = new XMLHttpRequest();
    request.open("POST", '/option_update/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
        option_id: option_id,
        option_quantity: option_quantity,
        option_price: option_price,
    };

    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
                if (document.querySelector('.basket-desktop-price')) {
                    document.querySelector('.basket-desktop-price').textContent = addSlicePrice(response.total_price);
                    document.querySelector('.basket-mobile-price').textContent = addSlicePrice(response.total_price);
                }
            }
        }
    }
}

function optionAddFunction(product_id) {
    let request = new XMLHttpRequest();
    request.open("POST", '/add_options/', true);
    request.setRequestHeader("Content-Type", "application/json");
    let data = {
        product_id: product_id,
        options: options,
    };

    request.send(JSON.stringify(data));

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                let response = JSON.parse(request.response);
            }
        }
    }
}


// ####################### UPDATE SCRIPT ################################

// ####################### UPDATE PRODUCT IN BASKET SCRIPT ################################

if (document.querySelectorAll('.default-select')) {
    document.querySelectorAll('.default-select').forEach(function (el, i) {
        document.querySelectorAll('.default-select')[i].addEventListener('click', function () {
            document.querySelectorAll('.default-select')[i].classList.toggle('active');
        })
    });
    let selectItem = document.querySelectorAll('.select-item');
    selectItem.forEach(function (el, i) {
        selectItem[i].addEventListener('click', function () {
            selectItem[i].closest('.default-select').querySelector('.check-select').textContent = selectItem[i].textContent;
            let productId = selectItem[i].closest('.product-basket-item').getAttribute('data-product-id');
            if (selectItem[i].closest('.time')) {
                let time = selectItem[i].getAttribute('data-text');
                let timePrice = selectItem[i].getAttribute('data-time');
                productTimeUpdateFunction(productId, time, timePrice)
            }
            if (selectItem[i].closest('.size')) {
                let size = selectItem[i].textContent;
                let sizePrice = selectItem[i].getAttribute('data-price');
                productSizeUpdateFunction(productId, size, sizePrice)
            }
        })
    })
}

// ####################### UPDATE PRODUCT IN BASKET SCRIPT ################################

// ####################### SEND FORM SCRIPT ################################


function inputValidate() {
    IMask(document.querySelector('.phone-mask'), {
        mask: '+{7} (000) 000 00 00',
    });
}

if (document.querySelector('input[name$="name"]')) {
    document.querySelector('input[name$="name"]').addEventListener('input', function () {
        document.querySelector('input[name$="name"]').closest('.default-input').classList.remove('input-error');
    });
}
if (document.querySelector('input[name$="phone"]')) {
    document.querySelector('input[name$="phone"]').addEventListener('input', function () {
        document.querySelector('input[name$="phone"]').closest('.default-input').classList.remove('input-error');
    });
}
if (document.querySelector('.footer-confirm-btn')) {
    document.querySelector('.footer-confirm-btn').addEventListener('click', function () {
        document.querySelector('.form-modal-agreement').classList.add('show')
    });
}
if (document.querySelector('.show-confirm-modal')) {
    document.querySelector('.show-confirm-modal').addEventListener('click', function () {
        document.querySelector('.form-modal-agreement').classList.add('show')
    });
}
if (document.querySelector('.send-feedback-form-btn')) {
    document.querySelector('.send-feedback-form-btn').addEventListener('click', function () {
        validateForm('main-feedback-form');
    });
}
if (document.querySelector('.send-order-form-btn')) {
    document.querySelector('.send-order-form-btn').addEventListener('click', function () {
        validateForm('order-form');
    });
}

function validateForm(className) {
    let form = document.querySelector('.' + className);
    let formName = form.querySelector('input[name$="name"]');
    let formTelephone = form.querySelector('input[name$="phone"]');
    if (formName.value === '') {
        formName.closest('.default-input').classList.add('input-error');
    } else if (formTelephone.value.length < 18) {
        formTelephone.closest('.default-input').classList.add('input-error');
    } else {
        sendFormFunction(className)
    }
}

function sendFormFunction(className) {
    let request = new XMLHttpRequest();
    let sendForm = document.querySelector('.' + className);
    request.open('POST', sendForm.getAttribute('action'), true);

    let formData = new FormData(sendForm);
    request.send(formData);

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                if (document.querySelector('.order-modal')) {
                    document.querySelector('.order-modal').classList.remove('show');
                }
                if (document.querySelector('.feedback-modal')) {
                    document.querySelector('.feedback-modal').classList.remove('show');
                }
                document.querySelector('.form-success').classList.add('show');
                setTimeout(function () {
                    document.querySelector('.form-success').classList.remove('show');
                    if (className !== 'main-feedback-form') {
                        document.location.reload();
                    }
                }, 2500);
            } else {
                if (document.querySelector('.order-modal')) {
                    document.querySelector('.order-modal').classList.remove('show');
                }
                if (document.querySelector('.feedback-modal')) {
                    document.querySelector('.feedback-modal').classList.remove('show');
                }
                document.querySelector('.form-danger').classList.add('show');
                setTimeout(function () {
                    document.querySelector('.form-danger').classList.remove('show');
                }, 2500);
            }
        }
    }
}

document.querySelectorAll('.basket-total-btn').forEach(function (el, i) {
    document.querySelectorAll('.basket-total-btn')[i].addEventListener('click', function () {
        document.querySelector('.order-modal').classList.add('show');
        inputValidate()
    })
});


// ####################### SEND FORM SCRIPT ################################
// ####################### CLEAR CART SCRIPT ################################
if (document.querySelector('.basket-clear')) {
    document.querySelector('.basket-clear').addEventListener('click', function () {
        document.querySelector('.clear-cart-modal').classList.add('show')
    });
}

if (document.querySelector('.clear-cart-btn')) {
    document.querySelector('.clear-cart-btn').addEventListener('click', function () {
        let request = new XMLHttpRequest();
        request.open("GET", '/clear_cart/', true);
        request.setRequestHeader("Content-Type", "application/json");

        request.send();

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200 && request.status < 300) {
                    document.querySelectorAll('.modal').forEach(function (el, i) {
                        document.location.reload();
                        document.querySelectorAll('.modal')[i].classList.remove('show');
                    })
                }
            }
        }
    });
}

// ####################### CLEAR CART SCRIPT ################################

// ####################### SORT FIXED SCRIPT ################################

let sort = document.querySelector('.catalog-filter-fixed');
let productBlock = document.querySelector('.product-cart-block');

if (productBlock && window.innerWidth <= 960) {
    window.addEventListener('scroll', () => {
        let fixmeTop = window.pageYOffset + productBlock.getBoundingClientRect().top;
        let currentScroll = this.scrollY;
        if (currentScroll >= fixmeTop && currentScroll < (fixmeTop + productBlock.offsetHeight)) {
            if (sort) {
                sort.classList.add('fixed');
            }
        } else {
            if (sort) {
                sort.classList.remove('fixed');
            }
        }
    });
}

// ####################### SORT FIXED SCRIPT ################################

// ####################### SEARCH SCRIPT ################################


document.querySelector('.search-form-input').addEventListener('input', function () {
    document.querySelector('.search-result').classList.add('show');
    let search = document.querySelector('.search-form-input').value;
    let request = new XMLHttpRequest();
    request.open("GET", '/search_product/?search=' + search, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.send();

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200 && request.status < 300) {
                document.querySelector('.search-result').innerHTML = request.response;
            } else {
                document.querySelector('.search-result').classList.remove('show');
            }
        }
    }
});
// document.addEventListener('click', function (event) {
//         if (event.target.closest('.search-link')) {
//             document.querySelector('.search-desktop').classList.add('show');
//         } else if (!event.target.closest('.search-desktop')) {
//             document.querySelector('.search-result').classList.remove('show');
//             document.querySelector('.search-desktop').classList.remove('show');
//         }
//     }
// );
// if (document.querySelector('.search-link')) {
//     document.querySelector('.search-link').addEventListener('click', function () {
//         document.querySelector('.search-desktop').classList.toggle('show');
//     });
// }
if (document.querySelector('.close-search')) {
    document.querySelector('.close-search').addEventListener('click', function () {
        document.querySelector('.search-desktop').classList.remove('show');
    });
}
// ####################### SEARCH SCRIPT ################################