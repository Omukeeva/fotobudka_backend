from django.apps import AppConfig


class ProductConfig(AppConfig):
    name = 'product'
    verbose_name = '1. Категории и вся инфа на странице категорий'
