from django.contrib import admin
# Register your models here.
from django.utils.html import format_html
from import_export.admin import ImportExportModelAdmin
from mptt.admin import DraggableMPTTAdmin

from product.models import FAQ, Category, Options, Product, SeoCategory, MetaCategory, Size, Slider, AboutCategory, WorkCategory, MetaProduct, Characteristic, Brand, BrandImages, \
    EventImages, Event, ResultBlock, BrandExample, SeoForCategory, SeoProduct


class SliderInline(admin.StackedInline):
    model = Slider
    extra = 1

    def image_tag(self, obj):
        return format_html('<img style="width: 50px; height: 50px;" src="{}" />'.format(obj.image.url))

    image_tag.short_description = 'Картинка'
    readonly_fields = ['image_tag']
    fields = (('image', 'is_gif', 'image_tag'), 'alt', 'order',)


class SizeInline(admin.StackedInline):
    model = Size
    extra = 1


class MetaProductInline(admin.StackedInline):
    model = MetaProduct
    extra = 1
    max_num = 1


class CharacteristicInline(admin.StackedInline):
    model = Characteristic
    extra = 1


class SeoProductInline(admin.StackedInline):
    model = SeoProduct
    extra = 1
    max_num = 1


class ProductAdmin(ImportExportModelAdmin):
    inlines = (SliderInline, SizeInline, CharacteristicInline, MetaProductInline, SeoProductInline,)
    prepopulated_fields = {"slug": ('title',)}
    list_display = ('title', 'category_list', 'popular',)
    search_fields = ('title',)

    class Meta:
        model = Product

    def category_list(self, obj):
        return "\n,".join([p.title for p in obj.category.all()])

    category_list.short_description = 'Категории'


class BrandImagesInline(admin.StackedInline):
    model = BrandImages
    extra = 1

    def image_tag(self, obj):
        return format_html('<img style="width: 50px; height: 50px;" src="{}" />'.format(obj.image.url))

    image_tag.short_description = 'Картинка'
    fields = (('image', 'image_tag'), 'alt', 'title', 'text')
    readonly_fields = ['image_tag']


class BrandAdmin(admin.ModelAdmin):
    inlines = (BrandImagesInline,)

    class Meta:
        model = Brand


class EventImagesInline(admin.StackedInline):
    model = EventImages
    extra = 1

    def image_tag(self, obj):
        return format_html('<img style="width: 50px; height: 50px;" src="{}" />'.format(obj.image.url))

    image_tag.short_description = 'Картинка'
    readonly_fields = ['image_tag']
    fields = (('image', 'image_tag'), 'alt')


class EventAdmin(admin.ModelAdmin):
    inlines = (EventImagesInline,)

    class Meta:
        model = Event


class OptionsAdmin(admin.ModelAdmin):
    list_display = ('title', 'order', 'price')

    class Meta:
        model = Options


class MetaCategoryInline(admin.StackedInline):
    model = MetaCategory
    extra = 1
    max_num = 1


class SeoCategoryInline(admin.StackedInline):
    model = SeoForCategory
    extra = 1


class BrandExampleInline(admin.StackedInline):
    model = BrandExample
    extra = 1
    max_num = 1


# class ResultBlockInline(admin.StackedInline):
#     model = ResultBlock
#     extra = 1
#     max_num = 1


class CategoryAdmin(ImportExportModelAdmin, DraggableMPTTAdmin):
    inlines = (BrandExampleInline, MetaCategoryInline, SeoCategoryInline,)

    class Meta:
        model = Category


# admin.site.register(Purpose)
admin.site.register(AboutCategory)
admin.site.register(WorkCategory)
admin.site.register(FAQ)
# admin.site.register(Complement)
# admin.site.register(BrandExample)
# admin.site.register(ResultBlock)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Options, OptionsAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(SeoCategory)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Event, EventAdmin)
