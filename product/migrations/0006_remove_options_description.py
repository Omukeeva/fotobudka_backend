# Generated by Django 2.2.6 on 2021-06-25 10:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_category_tab_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='options',
            name='description',
        ),
    ]
