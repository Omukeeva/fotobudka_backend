# Generated by Django 2.2.6 on 2021-10-12 08:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0022_auto_20211011_1449'),
    ]

    operations = [
        migrations.AddField(
            model_name='options',
            name='info',
            field=models.CharField(blank=True, help_text='Текст должен быть "4 ч", "1 ч"', max_length=50, null=True, verbose_name='Время'),
        ),
    ]
