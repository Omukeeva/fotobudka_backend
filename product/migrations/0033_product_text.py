# Generated by Django 2.2.6 on 2021-11-07 12:57

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0032_auto_20211107_1345'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Описание'),
        ),
    ]
