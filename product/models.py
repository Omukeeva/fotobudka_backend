from PIL import Image
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.files import File
from django.db import models
from io import BytesIO
# Create your models here.
from django.urls import reverse
from imagekit.models import ImageSpecField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from pilkit.processors import ResizeToFill


# def compress(image):
#     im = Image.open(image)
#     im_io = BytesIO()
#     if im.mode != 'RGBA' and im.mode != 'P':
#         im.save(im_io, 'JPEG', quality=85)
#         new_image = File(im_io, name=image.name)
#         return new_image
#     else:
#         return image


class Complement(models.Model):
    class Meta:
        verbose_name = 'Что в комплекте?'
        verbose_name_plural = 'Что в комплекте?'

    category = models.ManyToManyField(
        to='Category', verbose_name='Категория',
        related_name='complected'
    )
    title = models.CharField(max_length=255, verbose_name='Название')
    image = models.FileField(verbose_name='Иконка', null=True)
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return self.title


class Purpose(models.Model):
    class Meta:
        verbose_name = 'Для чего подойдет?'
        verbose_name_plural = 'Для чего подойдет?'

    category = models.ManyToManyField(to='Category', verbose_name='Категория', related_name='purpose')
    title = models.CharField(max_length=255, verbose_name='Название')
    image = models.FileField(verbose_name='Иконка')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    text = models.TextField(verbose_name='Мини текст')

    def __str__(self):
        return self.title


class FAQ(models.Model):
    class Meta:
        verbose_name = 'Вопросы и ответы'
        verbose_name_plural = 'Вопросы и ответы'

    category = models.ManyToManyField(
        to='Category', verbose_name='Категория',
        related_name='faq'
    )
    title = models.CharField(max_length=255, verbose_name='Вопрос')
    description = RichTextUploadingField(verbose_name='Ответ')

    def __str__(self):
        return self.title


class MetaCategory(models.Model):
    class Meta:
        verbose_name = 'Мета для категории'
        verbose_name_plural = 'Мета для категории'

    title = models.CharField(max_length=255, verbose_name='Мета title')
    description = models.TextField(verbose_name='Мета description')
    category = models.ForeignKey(
        to='Category', on_delete=models.SET_NULL, verbose_name='Категория',
        related_name='meta', null=True
    )

    def __str__(self):
        return self.title


class SeoCategory(models.Model):
    class Meta:
        verbose_name = 'Seo для категории'
        verbose_name_plural = 'Seo для категории'

    text = RichTextUploadingField(verbose_name='Seo текст')
    category = models.ForeignKey(
        to='Category', on_delete=models.SET_NULL, verbose_name='Категория',
        related_name='seo', null=True
    )

    def __str__(self):
        return 'Seo ' + str(self.pk)


class SeoProduct(models.Model):
    class Meta:
        verbose_name = 'Seo для продуктов'
        verbose_name_plural = 'Seo для продуктов'

    text = RichTextUploadingField(verbose_name='Seo текст')
    product = models.ForeignKey(
        to='Product', on_delete=models.SET_NULL, verbose_name='Продукт',
        related_name='seo', null=True
    )

    def __str__(self):
        return 'Seo ' + str(self.pk)


class SeoForCategory(models.Model):
    class Meta:
        verbose_name = 'Seo для категории'
        verbose_name_plural = 'Seo для категории'

    text = RichTextUploadingField(verbose_name='Seo текст')
    image = models.ImageField(verbose_name='Картинка')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    order = models.PositiveIntegerField(verbose_name='Порядок', null=True, default=0, help_text='Чем меньше цифра, тем выше она стоит')
    category = models.ForeignKey(
        to='Category', on_delete=models.SET_NULL, verbose_name='Категория',
        related_name='seo_all', null=True
    )

    def __str__(self):
        return 'Seo ' + str(self.pk)


class Category(MPTTModel):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    parent = TreeForeignKey(
        'self', on_delete=models.SET_NULL, null=True,
        blank=True, related_name='children',
        verbose_name='Родительская категория')
    title = models.CharField(max_length=255, verbose_name='Название h1', null=True)
    subtitle = models.CharField(
        max_length=255, verbose_name='Название h2', null=True)
    title_main = models.CharField(
        max_length=255, verbose_name='Название для главной', null=True)
    description = models.TextField(
        verbose_name='Описание карточки меню на главной', null=True)
    image_main = models.ImageField(
        verbose_name='Картинка меню на главной странице', upload_to='category_images', null=True
    )
    image_main_crop = ImageSpecField(
        source='image_main', processors=[ResizeToFill(336, 288)], format='JPEG', options={'quality': 100}
    )
    alt_main = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image_header = models.ImageField(
        verbose_name='Картинка выпадающего меню', upload_to='category_images', null=True
    )
    image_header_crop = ImageSpecField(
        source='image_header', processors=[ResizeToFill(156, 116)], format='JPEG', options={'quality': 100}
    )
    alt_header = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    text = models.TextField(
        verbose_name='Описание на внутренней странице', null=True
    )
    image = models.ImageField(
        verbose_name='Картинка на внутренней странице', upload_to='category_images', null=True
    )
    image_crop = ImageSpecField(
        source='image', processors=[ResizeToFill(600, 400)], format='JPEG', options={'quality': 100}
    )
    image_crop_small = ImageSpecField(
        source='image', processors=[ResizeToFill(360, 260)], format='JPEG', options={'quality': 100}
    )
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    btn_text = models.CharField(verbose_name='Текст для кнопки', max_length=255, null=True, blank=True, help_text='Текст должен быть таким "Выбрать фотобудку"/"Выбрать фотозону"/и т.д.')
    video = models.URLField(verbose_name='Ссылка на YouTube', blank=True, null=True)
    slug = models.CharField(verbose_name='Ярлык', unique=True, null=True, max_length=255)
    header = models.BooleanField(verbose_name='Вывести в шапке', default=True)
    tab_title = models.CharField(max_length=255, verbose_name='Короткое название для табов', null=True, blank=True)
    order = models.PositiveIntegerField(verbose_name='Порядок', null=True, default=0)

    def __str__(self):
        return self.title


class AboutCategory(models.Model):
    class Meta:
        verbose_name = 'Блок "Что такое ... ?"'
        verbose_name_plural = 'Блок "Что такое ... ?"'

    title = models.CharField(max_length=255, verbose_name='Название')
    description = RichTextUploadingField(verbose_name='Описание', help_text='Чтобы текст сделать розовым добавьте в тег "class="pink-text""')
    image1 = models.ImageField(verbose_name='Картинка №1', null=True, help_text='Размер картинки 645x400. Загружать либо 1 картинку, либо все 4')
    image1_crop = ImageSpecField(source='image1', processors=[ResizeToFill(645, 400)], format='JPEG', options={'quality': 100})
    alt1 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image2 = models.ImageField(verbose_name='Картинка №2', null=True, blank=True, help_text='Размер картинки 645x400.')
    image2_crop = ImageSpecField(source='image2', processors=[ResizeToFill(645, 400)], format='JPEG', options={'quality': 100})
    alt2 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image3 = models.ImageField(verbose_name='Картинка №3', null=True, blank=True, help_text='Размер картинки 645x400.')
    image3_crop = ImageSpecField(source='image3', processors=[ResizeToFill(645, 400)], format='JPEG', options={'quality': 100})
    alt3 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image4 = models.ImageField(verbose_name='Картинка №4', null=True, blank=True, help_text='Размер картинки 645x400.')
    image4_crop = ImageSpecField(source='image4', processors=[ResizeToFill(645, 400)], format='JPEG', options={'quality': 100})
    alt4 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    category = models.ForeignKey(to='Category', on_delete=models.SET_NULL, null=True, related_name='about')

    def __str__(self):
        return self.title


class WorkCategory(models.Model):
    class Meta:
        verbose_name = 'Блок "Как работает?"'
        verbose_name_plural = 'Блок "Как работает?"'

    text1 = models.TextField(verbose_name='Шаг №1')
    text2 = models.TextField(verbose_name='Шаг №2')
    text3 = models.TextField(verbose_name='Шаг №3')
    text4 = models.TextField(verbose_name='Результат')
    category = models.ForeignKey(to='Category', on_delete=models.SET_NULL, null=True, related_name='work')

    def __str__(self):
        return "Как работает " + self.category.title


class BrandExample(models.Model):
    class Meta:
        verbose_name = 'Примеры брендирования'
        verbose_name_plural = 'Примеры брендирования'

    title = models.CharField(max_length=255, verbose_name='Заголовок')
    category = models.ForeignKey(to='Category', on_delete=models.SET_NULL, null=True, related_name='brand_example')
    description = RichTextUploadingField(verbose_name='Текст')
    image1 = models.ImageField(verbose_name='Картинка № 1')
    alt1 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image2 = models.ImageField(verbose_name='Картинка № 2')
    alt2 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image3 = models.ImageField(verbose_name='Картинка № 3')
    alt3 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image4 = models.ImageField(verbose_name='Картинка № 4')
    alt4 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return self.title


class ResultBlock(models.Model):
    class Meta:
        verbose_name = 'Блок с результатами фотографий'
        verbose_name_plural = 'Блок с результатами фотографий'

    title = models.CharField(max_length=255, verbose_name='Заголовок')
    category = models.ForeignKey(to='Category', on_delete=models.SET_NULL, null=True, related_name='result_block')
    description = RichTextUploadingField(verbose_name='Текст')
    image1 = models.ImageField(verbose_name='Картинка № 1')
    alt1 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image2 = models.ImageField(verbose_name='Картинка № 2')
    alt2 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image3 = models.ImageField(verbose_name='Картинка № 3')
    alt3 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    image4 = models.ImageField(verbose_name='Картинка № 4')
    alt4 = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return self.title


class Event(models.Model):
    class Meta:
        verbose_name = 'Фотографии на мероприятиях'
        verbose_name_plural = 'Фотографии на мероприятиях'

    title = models.CharField(max_length=255, verbose_name='Заголовок')
    category = models.ForeignKey(to='Category', on_delete=models.SET_NULL, null=True, related_name='event')

    def __str__(self):
        return self.title


class EventImages(models.Model):
    class Meta:
        verbose_name = 'Фотографии на мероприятиях'
        verbose_name_plural = 'Фотографии на мероприятиях'

    image = models.ImageField(verbose_name='Картинка')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    event = models.ForeignKey(to='Event', on_delete=models.SET_NULL, null=True, related_name='event_images')

    def __str__(self):
        return self.event.title + str(self.pk)


class Product(models.Model):
    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    category = models.ManyToManyField(
        to='Category', verbose_name='Категория',
        related_name='product'
    )
    category_slug = models.ForeignKey(
        to='Category', verbose_name='Категория для URL',
        related_name='product_slug', on_delete=models.SET_NULL, null=True, blank=True
    )
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(verbose_name='Описание', blank=True, null=True)
    text = RichTextUploadingField(verbose_name='Описание', blank=True, null=True)
    preview = models.ImageField(verbose_name='Превью картинка для видео', blank=True, null=True, help_text='Загружать фотографии размерами 550х340')
    preview_crop = ImageSpecField(source='preview', processors=[ResizeToFill(550, 367)], format='JPEG', options={'quality': 100})
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    video = models.URLField(verbose_name='Ссылка на видео YouTube', blank=True, null=True)
    price1 = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена за 1 час')
    price2 = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена за 2 часа')
    price3 = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена за 3 часа')
    price4 = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена за 5 часов')
    price5 = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена за 2 дня')
    price6 = models.DecimalField(max_digits=14, decimal_places=2, verbose_name='Цена за 3 дня')
    services = models.ManyToManyField(to='Options', related_name='services', verbose_name='Доп услуги')
    order = models.PositiveIntegerField(verbose_name='Порядок', null=True, default=0)
    popular = models.BooleanField(verbose_name='Вывести в рекомендуемые?', default=False)
    slug = models.CharField(verbose_name='Ярлык', unique=True, null=True, max_length=255, blank=True)

    def __str__(self):
        return self.title

    def product_url(self):
        if self.category_slug:
            return reverse('product', kwargs={'category_slug': self.category_slug.slug, 'slug': self.slug})
        else:
            return reverse('product', kwargs={'category_slug': self.category.first().slug, 'slug': self.slug})


class Size(models.Model):
    class Meta:
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'

    product = models.ForeignKey(
        to=Product, on_delete=models.SET_NULL, related_name='size',
        verbose_name='Продукт', null=True
    )
    size = models.CharField(max_length=100, verbose_name='Размер')
    price = models.DecimalField(
        max_digits=14, decimal_places=2, verbose_name='Цена', default=0,
        help_text='Для базового размера значение цены указывается - 0 (ноль)'
    )

    def __str__(self):
        return self.size


class Characteristic(models.Model):
    class Meta:
        verbose_name = 'Характеристика'
        verbose_name_plural = 'Характеристики'

    product = models.ForeignKey(
        to=Product, on_delete=models.SET_NULL, related_name='characteristic',
        verbose_name='Продукт', null=True
    )
    title = models.CharField(max_length=100, verbose_name='Название')
    icon = models.FileField(verbose_name='Иконка')
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)

    def __str__(self):
        return self.title


class Brand(models.Model):
    class Meta:
        verbose_name = 'Брендирование'
        verbose_name_plural = 'Брендирование'

    product = models.ManyToManyField(to=Product, related_name='brand', verbose_name='Продукт')
    title = models.CharField(max_length=255, verbose_name='Название')
    text = models.CharField(max_length=255, verbose_name='Описание', null=True, blank=True)

    def __str__(self):
        return self.title


class BrandImages(models.Model):
    class Meta:
        verbose_name = 'Брендирование'
        verbose_name_plural = 'Брендирование'

    brand = models.ForeignKey(to=Brand, related_name='brand', verbose_name='Брендирование', on_delete=models.SET_NULL, null=True)
    image = models.ImageField(verbose_name='Картинка', upload_to='slider_images', null=True)
    image_crop = ImageSpecField(source='image', processors=[ResizeToFill(376, 250)], format='JPEG', options={'quality': 100})
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    title = models.CharField(max_length=255, verbose_name='Заголовок', null=True, blank=True)
    text = models.TextField(verbose_name='Описание', null=True, blank=True)

    def __str__(self):
        return 'Брендирование ' + str(self.pk)


class Slider(models.Model):
    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайдер'

    product = models.ForeignKey(
        to=Product, on_delete=models.SET_NULL, related_name='slider',
        verbose_name='Продукт', null=True
    )
    image = models.ImageField(
        verbose_name='Картинка',
        upload_to='slider_images', null=True
    )
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    is_gif = models.BooleanField(verbose_name='Эта гифка?', default=False)
    image_crop = ImageSpecField(
        source='image', processors=[ResizeToFill(328, 220)],
        format='JPEG', options={'quality': 100}
    )
    image_crop_big = ImageSpecField(
        source='image', processors=[ResizeToFill(600, 400)],
        format='JPEG', options={'quality': 100}
    )
    image_crop_small = ImageSpecField(
        source='image', processors=[ResizeToFill(120, 80)],
        format='JPEG', options={'quality': 100}
    )
    order = models.PositiveIntegerField(verbose_name='Порядок', null=True, default=0)

    def __str__(self):
        return 'Слайдер - ' + str(self.pk)

    # def save(self, *args, **kwargs):
    #     new_image = compress(self.image_crop)
    #     self.image_crop = new_image
    #     new_image_big = compress(self.image)
    #     self.image = new_image_big
    #     super().save(*args, **kwargs)


class MetaProduct(models.Model):
    class Meta:
        verbose_name = 'Мета для продукта'
        verbose_name_plural = 'Мета для продукта'

    title = models.CharField(max_length=255, verbose_name='Мета title')
    description = models.TextField(verbose_name='Мета description')
    category = models.ForeignKey(
        to='Product', on_delete=models.SET_NULL, verbose_name='Продукт',
        related_name='meta', null=True
    )

    def __str__(self):
        return self.title


class Options(models.Model):
    class Meta:
        verbose_name = 'Доп. услуги'
        verbose_name_plural = 'Доп. услуги'

    CATEGORY_CHOICES = (
        (0, 'Персонал'),
        (1, 'Украшение'),
        (2, 'Оборудование'),
        (3, 'Услуги'),
        (4, 'Остальное'),
    )

    title = models.CharField(max_length=255, verbose_name='Название', help_text='Ограничение 40 символов')
    description = models.TextField(verbose_name='Описание', help_text='Ограничение 40 символов', null=True, blank=True)
    info = models.CharField(max_length=50, verbose_name='Время', null=True, blank=True,
                            help_text='Текст должен быть "4 ч", "1 ч"')
    image = models.ImageField(
        verbose_name='Картинка',
        upload_to='services_images', null=True
    )
    image_crop = ImageSpecField(
        source='image', processors=[ResizeToFill(600, 400)],
        format='JPEG', options={'quality': 100}
    )
    image_crop_small = ImageSpecField(
        source='image', processors=[ResizeToFill(265, 176)],
        format='JPEG', options={'quality': 100}
    )
    image_crop_basket = ImageSpecField(
        source='image', processors=[ResizeToFill(120, 80)],
        format='JPEG', options={'quality': 100}
    )
    alt = models.CharField(max_length=255, verbose_name='Alt', null=True, blank=True)
    price = models.DecimalField(
        max_digits=14, decimal_places=2, verbose_name='Базовая цена'
    )
    quantity = models.BooleanField(
        verbose_name='Выбор количества', default=True,
        help_text='Если услуга в единственном экземпляре, уберите галочку'
    )
    category = models.PositiveSmallIntegerField(
        default=0, choices=CATEGORY_CHOICES, null=True, blank=False,
        verbose_name='Категории'
    )
    order = models.PositiveIntegerField(verbose_name='Порядок', null=True, default=0)

    def __str__(self):
        return self.title

    # def save(self, *args, **kwargs):
    #     new_image = compress(self.image)
    #     self.image = new_image
    #     super().save(*args, **kwargs)
